object Form1: TForm1
  Left = 420
  Top = 154
  Width = 1073
  Height = 679
  Caption = 'DriveIOgui 2v4'
  Color = 2039583
  Constraints.MinHeight = 520
  Constraints.MinWidth = 1040
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  OnMouseDown = FormMouseDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 16
  object Memo1: TMemo
    Left = 16
    Top = 424
    Width = 289
    Height = 201
    TabStop = False
    Color = clBlack
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clLime
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 7
  end
  object Button1: TButton
    Left = 120
    Top = 384
    Width = 81
    Height = 25
    Caption = 'START'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button3: TButton
    Left = 32
    Top = 384
    Width = 73
    Height = 25
    Caption = 'Console'
    TabOrder = 5
    TabStop = False
    OnClick = Button3Click
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 9
    Width = 289
    Height = 88
    Caption = 'Drive settings'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    TabStop = True
    object LabelDrive1: TLabel
      Left = 16
      Top = 48
      Width = 77
      Height = 16
      Caption = 'LBA quantity:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LabelDrive2: TLabel
      Left = 16
      Top = 24
      Width = 72
      Height = 16
      Caption = 'Drive name:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object ComboBoxDrive1: TComboBox
      Left = 104
      Top = 24
      Width = 169
      Height = 24
      Color = clWindowText
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 16
      ParentFont = False
      TabOrder = 0
      OnChange = ComboBoxDrive1Change
      OnDblClick = ComboBoxDrive1DblClick
      OnDropDown = ComboBoxDrive1DropDown
    end
    object EditDrive1: TEdit
      Left = 104
      Top = 48
      Width = 169
      Height = 24
      Color = clNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object Button2: TButton
    Left = 216
    Top = 384
    Width = 73
    Height = 25
    Caption = 'Pause'
    Enabled = False
    TabOrder = 1
    TabStop = False
    OnClick = Button2Click
  end
  object PageControl1: TPageControl
    Left = 16
    Top = 112
    Width = 289
    Height = 257
    ActivePage = TabSheet1
    MultiLine = True
    TabIndex = 0
    TabOrder = 8
    object TabSheet1: TTabSheet
      Caption = 'Access'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      object LabelAccess1: TLabel
        Left = 16
        Top = 16
        Width = 87
        Height = 16
        Caption = 'Req. size [KB]:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelAccess2: TLabel
        Left = 16
        Top = 40
        Width = 58
        Height = 16
        Caption = 'Start LBA:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelAccess3: TLabel
        Left = 16
        Top = 64
        Width = 84
        Height = 16
        Caption = 'LBAs quantity:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelAccess4: TLabel
        Left = 16
        Top = 88
        Width = 99
        Height = 16
        Caption = 'Iteration quantity:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object EditAccess1: TEdit
        Left = 128
        Top = 16
        Width = 137
        Height = 24
        Color = clNone
        TabOrder = 0
        Text = '1024'
      end
      object EditAccess2: TEdit
        Left = 128
        Top = 40
        Width = 137
        Height = 24
        Color = clNone
        TabOrder = 1
        Text = '0'
      end
      object EditAccess3: TEdit
        Left = 128
        Top = 64
        Width = 137
        Height = 24
        Color = clNone
        TabOrder = 2
        Text = '0'
        OnDblClick = EditAccess3DblClick
      end
      object GroupBoxAccess1: TGroupBox
        Left = 16
        Top = 120
        Width = 113
        Height = 73
        Caption = 'Access type'
        TabOrder = 4
        object RadioButtonAccess1: TRadioButton
          Left = 16
          Top = 16
          Width = 89
          Height = 25
          Caption = 'Sequence'
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object RadioButtonAccess2: TRadioButton
          Left = 16
          Top = 40
          Width = 89
          Height = 25
          Caption = 'Random'
          TabOrder = 1
        end
      end
      object GroupBoxAccess2: TGroupBox
        Left = 152
        Top = 120
        Width = 113
        Height = 73
        Caption = 'Access dir'
        TabOrder = 5
        object RadioButtonAccess3: TRadioButton
          Left = 16
          Top = 16
          Width = 89
          Height = 25
          Caption = 'Read'
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object RadioButtonAccess4: TRadioButton
          Left = 16
          Top = 40
          Width = 89
          Height = 25
          Caption = 'Write'
          TabOrder = 1
        end
      end
      object EditAccess4: TEdit
        Left = 128
        Top = 88
        Width = 137
        Height = 24
        Color = clNone
        TabOrder = 3
        Text = '1'
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Graph'
      ImageIndex = 2
      object LabelGraph1: TLabel
        Left = 24
        Top = 122
        Width = 84
        Height = 16
        Caption = 'Filter coef [%]:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelGraph2: TLabel
        Left = 24
        Top = 170
        Width = 105
        Height = 16
        Caption = 'Update time [ms]:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object GroupBoxGraph1: TGroupBox
        Left = 16
        Top = 8
        Width = 113
        Height = 97
        Caption = 'X-Axis'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object RadioButtonGraph1: TRadioButton
          Left = 16
          Top = 16
          Width = 89
          Height = 25
          Caption = 'Time'
          TabOrder = 0
          OnClick = RadioButtonGraph1Click
        end
        object RadioButtonGraph2: TRadioButton
          Left = 16
          Top = 40
          Width = 89
          Height = 25
          Caption = 'Memory'
          Checked = True
          TabOrder = 1
          TabStop = True
          OnClick = RadioButtonGraph2Click
        end
        object RadioButtonGraph3: TRadioButton
          Left = 16
          Top = 64
          Width = 89
          Height = 25
          Caption = 'LBAs'
          TabOrder = 2
          OnClick = RadioButtonGraph3Click
        end
      end
      object GroupBoxGraph2: TGroupBox
        Left = 152
        Top = 8
        Width = 113
        Height = 49
        Caption = 'Y-Axis'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object RadioButtonGraph4: TRadioButton
          Left = 16
          Top = 16
          Width = 89
          Height = 25
          Caption = 'Speed'
          Checked = True
          Enabled = False
          TabOrder = 0
          TabStop = True
        end
      end
      object CheckBoxGraph1: TCheckBox
        Left = 160
        Top = 64
        Width = 97
        Height = 25
        Caption = ' Show T [C]'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object EditGraph1: TEdit
        Left = 160
        Top = 120
        Width = 105
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        Text = '20'
      end
      object CheckBoxGraph2: TCheckBox
        Left = 160
        Top = 88
        Width = 105
        Height = 25
        Caption = ' Show UART'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = CheckBoxGraph2Click
      end
      object CheckBoxGraph3: TCheckBox
        Left = 24
        Top = 142
        Width = 129
        Height = 25
        Caption = 'Auto scroll range:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
      object EditGraph2: TEdit
        Left = 160
        Top = 144
        Width = 105
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        Text = '5000'
      end
      object EditGraph3: TEdit
        Left = 160
        Top = 168
        Width = 105
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        Text = '64'
        OnChange = EditGraph3Change
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Statistic'
      ImageIndex = 3
      object LabelStat1: TLabel
        Left = 16
        Top = 16
        Width = 54
        Height = 16
        Caption = 'Time left:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelStat2: TLabel
        Left = 16
        Top = 40
        Width = 72
        Height = 16
        Caption = 'Memory left:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelStat3: TLabel
        Left = 16
        Top = 64
        Width = 97
        Height = 16
        Caption = 'Average speed:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelStat4: TLabel
        Left = 16
        Top = 88
        Width = 96
        Height = 16
        Caption = 'Write data [hex]:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelStat5: TLabel
        Left = 16
        Top = 112
        Width = 99
        Height = 16
        Caption = 'Read data [hex]:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelStat6: TLabel
        Left = 16
        Top = 136
        Width = 91
        Height = 16
        Caption = 'Fail check size:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelStat7: TLabel
        Left = 8
        Top = 176
        Width = 40
        Height = 16
        Caption = 'Marks:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object EditStat1: TEdit
        Left = 128
        Top = 16
        Width = 137
        Height = 24
        TabStop = False
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Text = '00:00:00'
      end
      object EditStat2: TEdit
        Left = 128
        Top = 40
        Width = 137
        Height = 24
        TabStop = False
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = '0 MB'
      end
      object EditStat3: TEdit
        Left = 128
        Top = 64
        Width = 137
        Height = 24
        TabStop = False
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        Text = '0 MB/s'
      end
      object EditStat4: TEdit
        Left = 128
        Top = 88
        Width = 137
        Height = 24
        TabStop = False
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        Text = '0x1A2B3C4D'
        OnDblClick = Edit4DblClick
      end
      object EditStat5: TEdit
        Left = 128
        Top = 112
        Width = 137
        Height = 24
        TabStop = False
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnDblClick = Edit4DblClick
      end
      object CheckBoxStat1: TCheckBox
        Left = 56
        Top = 176
        Width = 73
        Height = 17
        Caption = 'Raw'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
      object CheckBoxStat3: TCheckBox
        Left = 160
        Top = 176
        Width = 65
        Height = 17
        Caption = 'Time'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object EditStat6: TEdit
        Left = 128
        Top = 136
        Width = 137
        Height = 24
        TabStop = False
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        OnDblClick = Edit4DblClick
      end
      object CheckBoxStat2: TCheckBox
        Left = 108
        Top = 176
        Width = 49
        Height = 17
        Caption = 'Text'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
      end
      object CheckBoxStat4: TCheckBox
        Left = 216
        Top = 176
        Width = 57
        Height = 17
        Caption = 'Check'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Service'
      ImageIndex = 33
      object LabelService1: TLabel
        Left = 16
        Top = 16
        Width = 58
        Height = 16
        Caption = 'Start LBA:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelService2: TLabel
        Left = 16
        Top = 40
        Width = 77
        Height = 16
        Caption = 'LBA quantity:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object EditService1: TEdit
        Left = 128
        Top = 16
        Width = 137
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Text = '0'
      end
      object EditService2: TEdit
        Left = 128
        Top = 40
        Width = 137
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = '1024000'
        OnDblClick = EditService2DblClick
      end
      object ButtonService2: TButton
        Left = 104
        Top = 80
        Width = 81
        Height = 25
        Caption = 'Send TRIM'
        TabOrder = 2
        OnClick = ButtonService2Click
      end
      object ButtonService1: TButton
        Left = 16
        Top = 80
        Width = 81
        Height = 25
        Caption = 'Get SMART'
        TabOrder = 3
        OnClick = ButtonService1Click
      end
      object ButtonService3: TButton
        Left = 192
        Top = 80
        Width = 81
        Height = 25
        Caption = 'Clean disk'
        TabOrder = 4
        OnClick = ButtonService3Click
      end
      object ButtonService4: TButton
        Left = 16
        Top = 112
        Width = 81
        Height = 25
        Caption = 'Block Erase'
        TabOrder = 5
        OnClick = ButtonService4Click
      end
      object ButtonService5: TButton
        Left = 104
        Top = 112
        Width = 81
        Height = 25
        Caption = 'Crypto Erase'
        TabOrder = 6
        OnClick = ButtonService5Click
      end
      object ButtonService6: TButton
        Left = 192
        Top = 112
        Width = 81
        Height = 25
        Caption = 'Status'
        TabOrder = 7
        OnClick = ButtonService6Click
      end
      object ButtonService7: TButton
        Left = 16
        Top = 144
        Width = 81
        Height = 25
        Caption = 'Identify'
        TabOrder = 8
        OnClick = ButtonService7Click
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'UART'
      ImageIndex = 3
      object LabelUart1: TLabel
        Left = 16
        Top = 16
        Width = 59
        Height = 16
        Caption = 'COM port:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelUart2: TLabel
        Left = 16
        Top = 40
        Width = 61
        Height = 16
        Caption = 'Baud rate:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelUart3: TLabel
        Left = 16
        Top = 64
        Width = 37
        Height = 16
        Caption = 'Parity:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelUart4: TLabel
        Left = 16
        Top = 88
        Width = 55
        Height = 16
        Caption = 'Stop bits:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LabelUart5: TLabel
        Left = 16
        Top = 112
        Width = 73
        Height = 16
        Caption = 'Value index:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object ComboBoxUart1: TComboBox
        Left = 128
        Top = 16
        Width = 137
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 16
        ParentFont = False
        TabOrder = 0
        OnDropDown = ComboBoxUart1DropDown
      end
      object ComboBoxUart2: TComboBox
        Left = 128
        Top = 40
        Width = 137
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 16
        ItemIndex = 8
        ParentFont = False
        TabOrder = 1
        Text = '921600'
        Items.Strings = (
          '9600'
          '19200'
          '38400'
          '57600'
          '74880'
          '115200'
          '230400'
          '460800'
          '921600')
      end
      object ComboBoxUart3: TComboBox
        Left = 128
        Top = 64
        Width = 137
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 16
        ItemIndex = 0
        ParentFont = False
        TabOrder = 2
        Text = 'None'
        Items.Strings = (
          'None'
          'Odd'
          'Even')
      end
      object ButtonUart1: TButton
        Left = 104
        Top = 160
        Width = 81
        Height = 25
        Caption = 'Open'
        TabOrder = 3
        OnClick = ButtonUart1Click
      end
      object ComboBoxUart4: TComboBox
        Left = 128
        Top = 88
        Width = 137
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 16
        ItemIndex = 2
        ParentFont = False
        TabOrder = 4
        Text = '2'
        Items.Strings = (
          '1'
          '1,5'
          '2')
      end
      object EditUart1: TEdit
        Left = 128
        Top = 112
        Width = 137
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        Text = '0'
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Find'
      ImageIndex = 4
      object LabelFind1: TLabel
        Left = 24
        Top = 173
        Width = 42
        Height = 16
        Caption = 'Sector:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object MemoFind1: TMemo
        Left = 16
        Top = 33
        Width = 249
        Height = 136
        TabStop = False
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Lines.Strings = (
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C '
          '2B 1A 4D 3C 2B 1A 4D 3C 2B 1A 4D 3C 2B 1A '
          '4D 3C 2B 1A 4D 3C 2B 1A')
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object EditFind1: TEdit
        Left = 88
        Top = 168
        Width = 177
        Height = 24
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object CheckBoxFind1: TCheckBox
        Left = 24
        Top = 8
        Width = 225
        Height = 17
        Caption = 'Finding sector with data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
    end
  end
  object Chart1: TChart
    Left = 336
    Top = 16
    Width = 689
    Height = 609
    BackWall.Brush.Color = clWhite
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.AutomaticMinimum = False
    BottomAxis.Axis.Color = clLime
    BottomAxis.ExactDateTime = False
    BottomAxis.Grid.Color = 20480
    BottomAxis.Increment = 1
    BottomAxis.LabelsFont.Charset = DEFAULT_CHARSET
    BottomAxis.LabelsFont.Color = clLime
    BottomAxis.LabelsFont.Height = -13
    BottomAxis.LabelsFont.Name = 'Arial'
    BottomAxis.LabelsFont.Style = []
    BottomAxis.Maximum = 20
    BottomAxis.MinorGrid.Color = 20480
    BottomAxis.MinorTickCount = 1
    BottomAxis.MinorTickLength = 4
    BottomAxis.MinorTicks.Color = clLime
    BottomAxis.TickLength = 8
    BottomAxis.Ticks.Color = clLime
    BottomAxis.Title.Caption = 'Read / Written [MB]'
    BottomAxis.Title.Font.Charset = DEFAULT_CHARSET
    BottomAxis.Title.Font.Color = clLime
    BottomAxis.Title.Font.Height = -13
    BottomAxis.Title.Font.Name = 'Arial'
    BottomAxis.Title.Font.Style = []
    BottomAxis.TitleSize = 1
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Axis.Color = clLime
    LeftAxis.ExactDateTime = False
    LeftAxis.Grid.Color = 20480
    LeftAxis.Increment = 0.1
    LeftAxis.LabelsFont.Charset = DEFAULT_CHARSET
    LeftAxis.LabelsFont.Color = clLime
    LeftAxis.LabelsFont.Height = -13
    LeftAxis.LabelsFont.Name = 'Arial'
    LeftAxis.LabelsFont.Style = []
    LeftAxis.Maximum = 32
    LeftAxis.MinorGrid.Color = 20480
    LeftAxis.MinorTickCount = 1
    LeftAxis.MinorTickLength = 4
    LeftAxis.MinorTicks.Color = clLime
    LeftAxis.TickLength = 8
    LeftAxis.Ticks.Color = clLime
    LeftAxis.Title.Caption = 'Read / Write speed [MB/s]'
    LeftAxis.Title.Font.Charset = DEFAULT_CHARSET
    LeftAxis.Title.Font.Color = clLime
    LeftAxis.Title.Font.Height = -13
    LeftAxis.Title.Font.Name = 'Arial'
    LeftAxis.Title.Font.Style = []
    LeftAxis.TitleSize = 1
    Legend.Visible = False
    RightAxis.Automatic = False
    RightAxis.AutomaticMaximum = False
    RightAxis.AutomaticMinimum = False
    RightAxis.Axis.Color = clAqua
    RightAxis.Grid.Color = clTeal
    RightAxis.LabelsFont.Charset = DEFAULT_CHARSET
    RightAxis.LabelsFont.Color = clAqua
    RightAxis.LabelsFont.Height = -13
    RightAxis.LabelsFont.Name = 'Arial'
    RightAxis.LabelsFont.Style = []
    RightAxis.Maximum = 1000
    RightAxis.MinorTickCount = 1
    RightAxis.MinorTickLength = 4
    RightAxis.MinorTicks.Color = clAqua
    RightAxis.TickLength = 8
    RightAxis.Ticks.Color = clAqua
    RightAxis.TicksInner.Color = clAqua
    RightAxis.Title.Angle = 90
    RightAxis.Title.Caption = 'Current [mA]'
    RightAxis.Title.Font.Charset = DEFAULT_CHARSET
    RightAxis.Title.Font.Color = clAqua
    RightAxis.Title.Font.Height = -13
    RightAxis.Title.Font.Name = 'Arial'
    RightAxis.Title.Font.Style = []
    RightAxis.Visible = False
    View3D = False
    Color = clBlack
    TabOrder = 6
    OnDblClick = Chart1DblClick
    object Series1: TPointSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      Pointer.Brush.Color = 34986
      Pointer.HorizSize = 3
      Pointer.InflateMargins = True
      Pointer.Pen.Visible = False
      Pointer.Style = psDiamond
      Pointer.VertSize = 3
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series2: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psDiamond
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series3: TPointSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      Pointer.Brush.Color = 159
      Pointer.HorizSize = 3
      Pointer.InflateMargins = True
      Pointer.Pen.Visible = False
      Pointer.Style = psDiamond
      Pointer.VertSize = 3
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series4: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series5: TPointSeries
      Marks.ArrowLength = 0
      Marks.Visible = False
      SeriesColor = 8388863
      Pointer.Brush.Color = 9502890
      Pointer.HorizSize = 3
      Pointer.InflateMargins = True
      Pointer.Pen.Visible = False
      Pointer.Style = psDiamond
      Pointer.VertSize = 3
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series6: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = 8388863
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series7: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clAqua
      VertAxis = aRightAxis
      Pointer.InflateMargins = True
      Pointer.Style = psDiamond
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series8: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = 33023
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object ScrollBar1: TScrollBar
    Left = 320
    Top = 16
    Width = 17
    Height = 609
    Kind = sbVertical
    Max = 4000
    Min = 1
    PageSize = 0
    Position = 600
    TabOrder = 2
    OnChange = ScrollBar1Change
  end
  object ScrollBar2: TScrollBar
    Left = 1024
    Top = 16
    Width = 17
    Height = 609
    Kind = sbVertical
    Max = 2000
    Min = 1
    PageSize = 0
    Position = 200
    TabOrder = 3
    OnChange = ScrollBar2Change
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 64
    OnTimer = Timer1Timer
    Top = 376
  end
end
