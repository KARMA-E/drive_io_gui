#include <stdio.h>
#include <time.h>

#include <unistd.h>

#include <drive.hpp>
#include <drive_list.hpp>


int main(int argc, char* argv[])
{
    DriveList driveList;
    driveList.ShowDriveList();
    
    int opt;
    uint8_t driveInd = DriveList::kInvalidIndex;
    bool writeEn = false;

    while ((opt = getopt(argc, argv, "i:w")) != -1) {
        switch (opt) {
        case 'i':
            printf("Index: %s\n", optarg);
            driveInd = stoi(optarg);
            break;
        case 'w':
            printf("Write enabled\n");
            writeEn = true;
            break;
        default:
            fprintf(stderr, "Invalid options\n");
            return 1;
        }
    }
    
    string driveModel = driveList.GetModel(driveInd);

    if ((driveModel != "") && (driveInd != DriveList::kInvalidIndex))
    {
        //uint64_t driveLbaQty = driveList.GetLbaQty(driveModel);
        //printf("\nOpen drive \"%s\" lba qty %llu\n", driveModel.c_str(), driveLbaQty);
        Drive drive(driveInd);

        Drive::AccessParam_s accessParam =
        {
            .startLba       = 0,
            .lbaRange       = 1024 * 300,
            .lbaQty         = 1024 * 300,
            .itrQty         = 1,
            .reqBufSize     = 1024 * Drive::kSectorSize,
            .reqPattern     = 0x11223344,
            .randEn         = false,
            .dirWrite       = false,
            .markRawLba     = false,
            .markStrLba     = false,
            .markTime       = false,
            .checkRawLba    = false,
        };
        drive.SetAccessParam(accessParam);
        drive.StartTest();
        while (!drive.IsTestDone()) 
        {
            Sleep(100);
            printf("Speed: %f MB/s\n", drive.GetSpeedSlice() / 1'000'000.0);
        }

        if (writeEn)
        {
            uint8_t dataBuf[Drive::kSectorSize * 5];
            memset(dataBuf, 0x22, sizeof(dataBuf));
            for (int i = 0; i < 10; i++)
            {
                drive.Write(3 + 10 * i, 5, dataBuf, true, true, true);
            }
            printf("write\n");
        }   
    }
    else
    {
        printf("ERR! Invalid device index: %u\n", driveInd);
        return 1;
    }
    return 0;
}
