#include <stdio.h>
#include <time.h>

#include <drive.hpp>
#include <drive_list.hpp>

#include <progress_bar.hpp>
#include <print_tools.hpp>


//#define DBG_MAIN_NL(formatStr, ...)  printf("main:\t\t" formatStr "\n", ##__VA_ARGS__);


static inline void TestDiffRequestSizes(Drive &drive, uint64_t driveLbaQty, uint32_t waitPerStep, bool dirWrite)
{
    const uint64_t kRangeSize = 1000 * 1024 * 1024;   
    const uint64_t kMaxReqSize = 64 * 1024 * 1024;      
    const time_t kLoadTimeSec = waitPerStep * CLOCKS_PER_SEC;

    Drive::AccessParam_s accessParam =
    {
        .startLba       = 0,
        .lbaRange       = 0,
        .lbaQty         = driveLbaQty,
        .itrQty         = 1,
        .reqBufSize     = 0,
        .reqPattern     = 0x11223344,
        .randEn         = true,
        .dirWrite       = dirWrite,
        .markRawLba     = false,
        .markStrLba     = false,
        .markTime       = false,
        .checkRawLba    = false,
    };

    for (uint64_t reqSize = 512; reqSize <= kMaxReqSize; reqSize *= 2) {
        accessParam.lbaRange    = kRangeSize / Drive::kSectorSize;
        accessParam.startLba   += accessParam.lbaRange;
        accessParam.reqBufSize  = reqSize;
        drive.SetAccessParam(accessParam);

        printf("Test lba %08llX request size ", accessParam.startLba);
        ShowSize(accessParam.reqBufSize, printf);
        printf("    \t");

        {
            ProgressBar<uint64_t> bar(0, kLoadTimeSec, 10);
            drive.StartTest();

            while((time_t)drive.GetTimeLeftMs() < kLoadTimeSec) 
            {
                bar.Push(drive.GetTimeLeftMs());
                Sleep(10);
            }

            bar.Push(kLoadTimeSec);
            drive.StopTest();
        }

        float speedBps = (float)drive.GetSpeedSlice();
        float iops = speedBps / reqSize;

        printf(" \t");
        {
            ProgressBar<uint64_t> bar(0, 600 * 1000 * 1000, 60);
            bar.Push(speedBps);
        }
        printf(" ");
        ShowSpeed(speedBps, printf);
        printf(" \t");
        {
            ProgressBar<uint64_t> bar(0, 10 * 1000, 60);
            bar.Push(iops);
        }
        printf(" ");
        ShowIops(iops, printf);
        printf("\n");
    }
}

static inline void TestDiffRangeSizes(Drive &drive, uint64_t driveLbaQty, uint32_t waitPerStep, bool dirWrite)
{
    const uint64_t kReqSize = 64 * 1024;
    const uint64_t kDriveSize = driveLbaQty * Drive::kSectorSize;
    const time_t kLoadTimeSec = waitPerStep * CLOCKS_PER_SEC;

    Drive::AccessParam_s accessParam =
    {
        .startLba       = 0,
        .lbaRange       = 0,
        .lbaQty         = driveLbaQty,
        .itrQty         = 1,
        .reqBufSize     = kReqSize,
        .reqPattern     = 0x11223344,
        .randEn         = true,
        .dirWrite       = dirWrite,
        .markRawLba     = false,
        .markStrLba     = false,
        .markTime       = false,
        .checkRawLba    = false,
    };

    for (uint64_t rangeSize = 100 * 1024 * 1024; rangeSize < kDriveSize; rangeSize *= 1.4){
        accessParam.startLba    = 0;
        accessParam.lbaRange    = rangeSize / Drive::kSectorSize;
        drive.SetAccessParam(accessParam);

        printf("Test lba %08llX range size ", accessParam.startLba);
        ShowSize(rangeSize, printf);
        printf("    \t");

        {
            ProgressBar<uint64_t> bar(0, kLoadTimeSec, 10);
            drive.StartTest();

            while((time_t)drive.GetTimeLeftMs() < kLoadTimeSec) 
            {
                bar.Push(drive.GetTimeLeftMs());
                Sleep(10);
            }

            bar.Push(kLoadTimeSec);
            drive.StopTest();
        }

        float speedBps = (float)drive.GetSpeedSlice();
        float iops = speedBps / kReqSize;

        printf(" \t");
        {
            ProgressBar<uint64_t> bar(0, 600 * 1000 * 1000, 60);
            bar.Push(speedBps);
        }
        printf(" ");
        ShowSpeed(speedBps, printf);
        printf(" \t");
        {
            ProgressBar<uint64_t> bar(0, 10 * 1000, 60);
            bar.Push(iops);
        }
        printf(" ");
        ShowIops(iops, printf);
        printf("\n");
    }
}

static inline void TestWrite(Drive &drive, uint64_t driveLbaQty)
{
    Drive::AccessParam_s accessParam =
    {
        .startLba       = 0,
        .lbaRange       = driveLbaQty,
        .lbaQty         = driveLbaQty,
        .itrQty         = 1,
        .reqBufSize     = 4 * 1024,
        .reqPattern     = 0x55667788,
        .randEn         = false,
        .dirWrite       = true,
        .markRawLba     = false,
        .markStrLba     = false,
        .markTime       = false,
        .checkRawLba    = false,
    };

    drive.SetAccessParam(accessParam);
    drive.StartTest();

    while (!drive.IsTestDone())
    {
        Sleep(500);

        float speedBps = (float)drive.GetSpeedSlice();
        printf("\nprocessed ");
        ShowSize(drive.GetProcessedSize(), printf);
        printf(" speed ");
        ShowSpeed(speedBps, printf);
        printf("  ");
        ProgressBar<float> bar(0, 600000000, 40);
        bar.Push(speedBps);
    }
}

//---------------------------------------------------------------------------------------------------------------------

int main()
{
    DriveList driveList;
    driveList.ShowDriveList();

    uint8_t driveInd = DriveList::kInvalidIndex;
    printf("Enter device index: ");
    scanf("%hhu", &driveInd);
    string driveModel = driveList.GetModel(driveInd);
    uint64_t driveLbaQty = driveList.GetLbaQty(driveModel);

    if (driveModel != "")
    {
        printf("Open drive \"%s\"\n", driveModel.c_str());
        Drive drive(driveInd);

        printf("\nStart diff request sizes load\n");
        TestDiffRequestSizes(drive, driveLbaQty, 4, true);

        printf("\nStart diff range sizes load\n");
        TestDiffRangeSizes(drive, driveLbaQty, 4, true);

        //Write(drive, driveLbaQty);
        printf("\n");
    }
    else
    {
        printf("Invalid device index\n");
    }

    return 0;
}
