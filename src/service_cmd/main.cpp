#include <stdio.h>
#include <time.h>

#include <drive.hpp>
#include <drive_list.hpp>

#include <progress_bar.hpp>
#include <print_tools.hpp>

#define REQUEST_TYPE_POS            (1)
#define DRIVE_INDEX_PARAM_POS       (2)

int main(int argc, char* argv[])
{
    DriveList driveList;
    driveList.ShowDriveList();

    if (argc < 3) {
        printf("\nERR! Need 2 parameters:\n");
        printf("    1 param: identify, sanitize or status\n");
        printf("    2 param: system drive index\n");
        return 1;
    }

    uint8_t driveInd = stoi(argv[DRIVE_INDEX_PARAM_POS]);
    string driveModel = driveList.GetModel(driveInd);
    uint64_t driveLbaQty = driveList.GetLbaQty(driveModel);

    if (driveModel != "")
    {
        printf("\nOpen drive \"%s\" lba qty %llu\n", driveModel.c_str(), driveLbaQty);
        Drive drive(driveInd);

        if (memcmp(argv[REQUEST_TYPE_POS], "identify", 8) == 0) {
            IDENTIFY_DEVICE_DATA identifyBuf;
            drive.GetIdentify(&identifyBuf);
        } else if (memcmp(argv[REQUEST_TYPE_POS], "sanitize", 8) == 0) {
            drive.SendScsiCommand(0xB4, 0x12, 0x426B4572);
        } else if (memcmp(argv[REQUEST_TYPE_POS], "status", 6) == 0) {
            drive.SendScsiCommand(0xB4, 0x00, 0x00);
        } else {
            printf("ERR! Invalid request type: \"%s\"", argv[REQUEST_TYPE_POS]);
            return 2;
        }
    }
    else
    {
        printf("ERR! Invalid device index: %u\n", driveInd);
        return 3;
    }

    printf("Finish\n");
    return 0;
}
