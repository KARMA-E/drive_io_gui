# Параметры проекта, общие для всех конфигураций сборки

# Выбор компиляции
ifeq ($(OS),Windows_NT)
	GCC     := gcc
	G++     := g++
	SIZE    := size
	OBJDUMP := objdump
	OBJCOPY := objcopy
else
	GCC     := x86_64-w64-mingw32-gcc
	G++     := x86_64-w64-mingw32-g++
	SIZE    := x86_64-w64-mingw32-size
	OBJDUMP := x86_64-w64-mingw32-objdump
	OBJCOPY := x86_64-w64-mingw32-objcopy
endif

# Название проекта
PRJ_NAME := drive_io

# Расширение исполняемого файла (".elf", ".exe", "" и др.)
PRJ_OUT_EXTENSION :=.exe

# Флаги компиляции
PRJ_CFLAGS := \

# Флаги линкера
PRJ_LFLAGS := \

# Дефайны
PRJ_DEFINES := \

# Пути к заголовочным файлам
PRJ_INC_PATHS := \
drive \
inc \

# Пути к файлам с исходным кодом
PRJ_SRC_PATHS := \
drive \
uart \

# Пути к скомпилированным библиотекам
PRJ_LIB_PATHS := \

# Наименования библиотек
PRJ_LIBRARIES := \

PRJ_GCC_DEF := \

# Прочие файлы, при изменении которых необходимо перезапускать сборку
PRJ_OTHER_DEPS := \
