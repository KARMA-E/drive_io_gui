//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Chart.hpp>
#include <ExtCtrls.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TChart *Chart1;
    TMemo *Memo1;
    TButton *Button1;
    TPointSeries *Series1;
    TLineSeries *Series4;
    TPointSeries *Series5;
    TLineSeries *Series6;
    TButton *Button3;
    TGroupBox *GroupBox1;
    TLabel *LabelDrive1;
    TLabel *LabelDrive2;
    TComboBox *ComboBoxDrive1;
    TEdit *EditDrive1;
    TLineSeries *Series2;
    TPointSeries *Series3;
    TLineSeries *Series7;
    TLineSeries *Series8;
    TButton *Button2;
    TScrollBar *ScrollBar2;
    TScrollBar *ScrollBar1;
    TPageControl *PageControl1;
    TTabSheet *TabSheet1;
    TLabel *LabelAccess1;
    TLabel *LabelAccess2;
    TLabel *LabelAccess3;
    TLabel *LabelAccess4;
    TEdit *EditAccess1;
    TEdit *EditAccess2;
    TEdit *EditAccess3;
    TGroupBox *GroupBoxAccess1;
    TRadioButton *RadioButtonAccess1;
    TRadioButton *RadioButtonAccess2;
    TGroupBox *GroupBoxAccess2;
    TRadioButton *RadioButtonAccess3;
    TRadioButton *RadioButtonAccess4;
    TEdit *EditAccess4;
    TTabSheet *TabSheet2;
    TLabel *LabelGraph1;
    TGroupBox *GroupBoxGraph1;
    TRadioButton *RadioButtonGraph1;
    TRadioButton *RadioButtonGraph2;
    TRadioButton *RadioButtonGraph3;
    TGroupBox *GroupBoxGraph2;
    TRadioButton *RadioButtonGraph4;
    TCheckBox *CheckBoxGraph1;
    TEdit *EditGraph1;
    TCheckBox *CheckBoxGraph2;
    TCheckBox *CheckBoxGraph3;
    TEdit *EditGraph2;
    TTabSheet *TabSheet3;
    TLabel *LabelService1;
    TLabel *LabelService2;
    TEdit *EditService1;
    TEdit *EditService2;
    TButton *ButtonService2;
    TButton *ButtonService1;
    TTabSheet *TabSheet4;
    TLabel *LabelUart1;
    TLabel *LabelUart2;
    TLabel *LabelUart3;
    TLabel *LabelUart4;
    TLabel *LabelUart5;
    TComboBox *ComboBoxUart1;
    TComboBox *ComboBoxUart2;
    TComboBox *ComboBoxUart3;
    TButton *ButtonUart1;
    TComboBox *ComboBoxUart4;
    TEdit *EditUart1;
    TTabSheet *TabSheet5;
    TLabel *LabelFind1;
    TMemo *MemoFind1;
    TEdit *EditFind1;
    TCheckBox *CheckBoxFind1;
    TTabSheet *TabSheet6;
    TLabel *LabelStat1;
    TEdit *EditStat1;
    TLabel *LabelStat2;
    TEdit *EditStat2;
    TLabel *LabelStat3;
    TEdit *EditStat3;
    TLabel *LabelStat4;
    TEdit *EditStat4;
    TTimer *Timer1;
    TLabel *LabelStat5;
    TEdit *EditStat5;
    TButton *ButtonService3;
    TLabel *LabelGraph2;
    TEdit *EditGraph3;
    TCheckBox *CheckBoxStat1;
    TCheckBox *CheckBoxStat2;
    TCheckBox *CheckBoxStat3;
    TCheckBox *CheckBoxStat4;
    TEdit *EditStat6;
    TLabel *LabelStat6;
    TButton *ButtonService4;
    TLabel *LabelStat7;
    TButton *ButtonService5;
    TButton *ButtonService6;
    TButton *ButtonService7;
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall Timer1Timer(TObject *Sender);
    void __fastcall ComboBoxDrive1DropDown(TObject *Sender);
    void __fastcall RadioButtonGraph1Click(TObject *Sender);
    void __fastcall RadioButtonGraph2Click(TObject *Sender);
    void __fastcall ComboBoxDrive1Change(TObject *Sender);
    void __fastcall EditAccess3DblClick(TObject *Sender);
    void __fastcall ButtonService2Click(TObject *Sender);
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall EditService2DblClick(TObject *Sender);
    void __fastcall FormResize(TObject *Sender);
    void __fastcall ComboBoxDrive1DblClick(TObject *Sender);
    void __fastcall ComboBoxUart1DropDown(TObject *Sender);
    void __fastcall ButtonUart1Click(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall ScrollBar2Change(TObject *Sender);
    void __fastcall Edit4DblClick(TObject *Sender);
    void __fastcall ScrollBar1Change(TObject *Sender);
    void __fastcall CheckBoxGraph2Click(TObject *Sender);
    void __fastcall Chart1DblClick(TObject *Sender);
    void __fastcall ButtonService1Click(TObject *Sender);
    void __fastcall RadioButtonGraph3Click(TObject *Sender);
    void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
    void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
    void __fastcall ButtonService3Click(TObject *Sender);
    void __fastcall EditGraph3Change(TObject *Sender);
    void __fastcall ButtonService4Click(TObject *Sender);
    void __fastcall ButtonService5Click(TObject *Sender);
    void __fastcall ButtonService6Click(TObject *Sender);
    void __fastcall ButtonService7Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
 