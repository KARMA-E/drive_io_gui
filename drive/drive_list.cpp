#include "drive_list.hpp"

#include <stdio.h>

#define DBG             printf
#define DBG_DLIST       printf(" drive_list: "); printf

#if (__GNUC__ < 12)
#define IOCTL_STORAGE_QUERY_PROPERTY   CTL_CODE(IOCTL_STORAGE_BASE, 0x0500, METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _STORAGE_DEVICE_DESCRIPTOR {
    ULONG Version;
    ULONG Size;
    UCHAR DriveType;
    UCHAR DriveTypeModifier;
    BOOLEAN RemovableMedia;
    BOOLEAN CommandQueueing;
    ULONG VendorIdOffset;
    ULONG ProductIdOffset;
    ULONG ProductRevisionOffset;
    ULONG SerialNumberOffset;
    STORAGE_BUS_TYPE BusType;
    ULONG RawPropertiesLength;
    UCHAR RawDriveProperties[1];
} STORAGE_DEVICE_DESCRIPTOR, *PSTORAGE_DEVICE_DESCRIPTOR;

typedef enum _STORAGE_PROPERTY_ID {
  StorageDeviceProperty = 0,
  StorageAdapterProperty,
  StorageDeviceIdProperty,
  StorageDeviceUniqueIdProperty,
  StorageDeviceWriteCacheProperty,
  StorageMiniportProperty,
  StorageAccessAlignmentProperty,
  StorageDeviceSeekPenaltyProperty,
  StorageDeviceTrimProperty,
  StorageDeviceWriteAggregationProperty,
  StorageDeviceDeviceTelemetryProperty,
  StorageDeviceLBProvisioningProperty,
  StorageDevicePowerProperty,
  StorageDeviceCopyOffloadProperty,
  StorageDeviceResiliencyProperty,
  StorageDeviceMediumProductType,
  StorageAdapterRpmbProperty,
  StorageAdapterCryptoProperty,
  StorageDeviceIoCapabilityProperty = 48,
  StorageAdapterProtocolSpecificProperty,
  StorageDeviceProtocolSpecificProperty,
  StorageAdapterTemperatureProperty,
  StorageDeviceTemperatureProperty,
  StorageAdapterPhysicalTopologyProperty,
  StorageDevicePhysicalTopologyProperty,
  StorageDeviceAttributesProperty,
  StorageDeviceManagementStatus,
  StorageAdapterSerialNumberProperty,
  StorageDeviceLocationProperty,
  StorageDeviceNumaProperty,
  StorageDeviceZonedDeviceProperty,
  StorageDeviceUnsafeShutdownCount,
  StorageDeviceEnduranceProperty,
  StorageDeviceLedStateProperty,
  StorageDeviceSelfEncryptionProperty = 64,
  StorageFruIdProperty
} STORAGE_PROPERTY_ID, *PSTORAGE_PROPERTY_ID;

typedef enum _STORAGE_QUERY_TYPE {
  PropertyStandardQuery     = 0,
  PropertyExistsQuery       = 1,
  PropertyMaskQuery         = 2,
  PropertyQueryMaxDefined   = 3 
} STORAGE_QUERY_TYPE, *PSTORAGE_QUERY_TYPE;

typedef struct _STORAGE_PROPERTY_QUERY {
  STORAGE_PROPERTY_ID PropertyId;
  STORAGE_QUERY_TYPE  QueryType;
  BYTE                AdditionalParameters[1];
} STORAGE_PROPERTY_QUERY, *PSTORAGE_PROPERTY_QUERY;
#endif


DriveList::DriveList()
{
    for(uint8_t driveInd = 0; driveInd < 255; driveInd++)
    {
        char indStr[5];
        sprintf(indStr, "%u", driveInd);
        string curDriveSysName = "\\\\.\\PHYSICALDRIVE";
        curDriveSysName += indStr;

        HANDLE curDriveHandle = CreateFile( curDriveSysName.c_str(),
                                            GENERIC_READ | GENERIC_WRITE,
                                            FILE_SHARE_READ | FILE_SHARE_WRITE,
                                            NULL,
                                            OPEN_EXISTING, 0, NULL);

        if((curDriveHandle != INVALID_HANDLE_VALUE) && (curDriveHandle != NULL))
        {
            ULONG returnedLength;
            UCHAR devDescBuf[kSectorSize];
            UCHAR query[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00};
            STORAGE_DEVICE_DESCRIPTOR *devDesc = (STORAGE_DEVICE_DESCRIPTOR*)devDescBuf;

            DeviceIoControl(curDriveHandle, IOCTL_STORAGE_QUERY_PROPERTY, &query, sizeof(query),
                            devDesc, kSectorSize, &returnedLength, NULL);

            string model = ((char*)devDesc + devDesc->ProductIdOffset);

            if (!CheckInIgnoreFile(model))
            {
                DISK_GEOMETRY geometry = {};
                DeviceIoControl(curDriveHandle, IOCTL_DISK_GET_DRIVE_GEOMETRY, NULL, 0,
                                &geometry, sizeof(geometry), &returnedLength, NULL);

                GET_LENGTH_INFORMATION realLength = {};
                DeviceIoControl(curDriveHandle, IOCTL_DISK_GET_LENGTH_INFO, NULL, 0,
                                &realLength, sizeof(realLength), &returnedLength, NULL);

                Properties_s curProperties;
                curProperties.index              = driveInd;
                curProperties.model              = model;
                curProperties.cylinders          = (uint64_t)geometry.Cylinders.QuadPart;
                curProperties.tracksPerCylinder  = geometry.TracksPerCylinder;
                curProperties.sectorsPerTrack    = geometry.SectorsPerTrack;
                curProperties.bytesPerSector     = geometry.BytesPerSector;
                if (geometry.BytesPerSector > 0) {
                    curProperties.lbaQty = (uint64_t)realLength.Length.QuadPart / geometry.BytesPerSector;
                } else {
                    curProperties.lbaQty = 0;
                }
                
                propertiesList.push_back(curProperties);
            }
        }
    }
    DBG_DLIST("create drive list\n");
}

DriveList::~DriveList()
{
    DBG_DLIST("delete drive list\n");
}

list<DriveList::Properties_s> DriveList::GetPropertiesList()
{
    return propertiesList;
}

string DriveList::GetModel(uint8_t index)
{
    list<DriveList::Properties_s>::iterator propertiesIt = propertiesList.begin();
    while (propertiesIt != propertiesList.end()) {
        if(index == propertiesIt->index) {
            return propertiesIt->model;
        }
        propertiesIt++;
    }
    return "";
}

uint8_t DriveList::GetIndex(string &driveModel)
{
    list<DriveList::Properties_s>::iterator propertiesIt = propertiesList.begin();
    while (propertiesIt != propertiesList.end()) {
        if(driveModel == propertiesIt->model) {
            return propertiesIt->index;
        }
        propertiesIt++;
    }
    return DriveList::kInvalidIndex;
}

uint64_t DriveList::GetLbaQty(string &driveModel)
{
    list<DriveList::Properties_s>::iterator propertiesIt = propertiesList.begin();
    while (propertiesIt != propertiesList.end()) {
        if(driveModel == propertiesIt->model) {
            return propertiesIt->lbaQty;
        }
        propertiesIt++;
    }
    return 0;
}

void DriveList::ShowDriveList()
{   
    DBG("\n");
    DBG_DLIST("%-8s %-10s %-12s %s\n", "Index", "LBA size", "LBA qty", "name");
    list<DriveList::Properties_s>::iterator propertiesIt = propertiesList.begin();
    while (propertiesIt != propertiesList.end()) {
        DBG_DLIST("%-8u %-10u %-12llu \"%s\"\n", 
                propertiesIt->index, 
                propertiesIt->bytesPerSector, 
                propertiesIt->lbaQty, 
                propertiesIt->model.c_str());
        propertiesIt++;
    }
    DBG("\n");
}

bool DriveList::CheckInIgnoreFile(string &driveModel)
{
    bool res = false;

    HANDLE fileHandle = CreateFile( IGNORE_FILE_NAME,
                                    GENERIC_READ | GENERIC_WRITE,
                                    FILE_SHARE_READ | FILE_SHARE_WRITE,
                                    NULL,
                                    OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL,
                                    NULL);

    if(fileHandle == INVALID_HANDLE_VALUE || fileHandle == NULL)
    {
        DBG_DLIST("File \"%s\" open fail !!!\n", IGNORE_FILE_NAME);
        res = true;
    }
    else
    {
        DBG_DLIST("Check driveModel \"%s\"", driveModel.c_str());

        DWORD fileSize = GetFileSize(fileHandle, NULL);
        char * const fileDataBuf = (char*)malloc(fileSize + 1);
        fileDataBuf[fileSize] = '\0';

        DWORD dwReadSize;
        ReadFile(fileHandle, fileDataBuf, fileSize, &dwReadSize, NULL);

        uint32_t driveModelSize = driveModel.length();
        uint32_t matchBytesQty = 0;

        for (uint32_t fileDataBufInd = 0; fileDataBufInd < fileSize; fileDataBufInd++)
        {            
            if (driveModel[matchBytesQty] == fileDataBuf[fileDataBufInd])
            {
                matchBytesQty++;

                if (matchBytesQty == driveModelSize)
                {
                    DBG(" - ignored");
                    res = true;
                    break;
                }
            }
            else
            {
                matchBytesQty = 0;
            }
        }
        DBG("\n");
        free(fileDataBuf);
    }

    CloseHandle(fileHandle);

    return res;
}
