#pragma once

#include <stdint.h>
#include <windows.h>
#include "ata.h"

#define DRIVE_ACCESS_MODE       (GENERIC_READ | GENERIC_WRITE)
#define DRIVE_ACCESS_SHARE      (FILE_SHARE_READ | FILE_SHARE_WRITE)

#define DRIVE_SECT_SIZE         (512UL)


#define SMART_ATTR_INVALID_ID   (0)
#define SMART_ATTR_THERMAL_ID   (194)
#define SMART_ATTR_MAX_ID       (255)

#pragma pack(push, 1)
typedef struct 
{
    uint8_t Id;
    uint16_t Flags;
    uint8_t Value;
    uint8_t Worst;
    uint8_t RawValue[6];
    uint8_t RES;

} smartAttribute_s;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct 
{
    uint16_t Version;
    uint8_t Data[1];
} smartBuf_s;
#pragma pack(pop)


class Drive
{
public:
    enum
    {
        kSectorSize = 512,
        kDriveSysNameSize = 256,
    };

    struct AccessParam_s
    {
        uint64_t startLba;
        uint64_t lbaRange;
        uint64_t lbaQty;
        uint32_t itrQty;
        uint32_t reqBufSize;
        uint32_t reqPattern;
        bool randEn;
        bool dirWrite;
        bool markRawLba;
        bool markStrLba;
        bool markTime;
        bool checkRawLba;
    };

private:
    HANDLE              driveHandle;
    HANDLE              threadHandle;
    uint8_t             index;
    char                driveSysName[kDriveSysNameSize];
    volatile uint64_t   processedSize;
    uint64_t            sliceProcessedSize;
    uint64_t            sliceTimeMs;
    volatile uint64_t   checkFailSize;
    AccessParam_s       accessParam;
    uint8_t             targetData[kSectorSize];
    bool                findTargetDataFlag;
    volatile uint64_t   targetDataSector;
    volatile uint64_t   startTimeMs;
    volatile bool       activeFlag;
    volatile bool       doneTrigger;
    OVERLAPPED          overLappedCtx;
    int (*dbgPrintf) (const char *str, ...);

public:
    Drive(uint8_t driveIndex);
    ~Drive();
    void SetDebugFunc(int (*func) (const char *str, ...));
    bool IsOpened();

    void Read(uint64_t lba, uint64_t lbaQty, uint8_t *reqBuf, bool checkRawLba);
    void Write(uint64_t lba, uint64_t lbaQty, uint8_t *reqBuf, bool markRawLba, bool markStrLba, bool markTime);

    void Trim(uint64_t startLba, uint64_t lbaQty);
    void Clean();

    void SetAccessParam(AccessParam_s &newAccessParam);
    AccessParam_s GetAccessParam();
    void StartTest();
    void SuspendTest();
    void ResumeTest();
    void StopTest();
    bool IsTestDone();

    uint64_t GetTimeLeftMs();
    uint64_t GetProcessedSize();
    uint64_t GetSpeedSlice();
    uint64_t GetCheckFailSize();
    uint32_t GetLastData();
    void     SetTargetData(uint8_t *targetData);
    uint64_t GetTargetSector();

    bool GetIdentify(IDENTIFY_DEVICE_DATA *identifyBuf);
    bool SendAtaCommand(uint8_t cmd, uint16_t feature, uint64_t lba);
    bool SendScsiCommand(uint8_t cmd, uint16_t feature, uint64_t lba);
    void GetSmart(smartAttribute_s *outSmartAttributes);
    int8_t GetTemperature();

private:
    void ReadLoad();
    void WriteLoad();
    static DWORD WINAPI TestThread(LPVOID lpParam);
    uint64_t GetRandomLbaOffset(uint64_t lbaInd);
    void TrimSectorBuf(void *trimBuf);
    void ResetCounters();
};

