#pragma once

#include <stdint.h>
#include <windows.h>

#include <list>
#include <string>
using namespace std;

#define IGNORE_FILE_NAME        "drive_ignore_list.txt"

class DriveList
{
public:
    enum {
        kSectorSize = 512,
        kInvalidIndex = 0xFF
    };

    struct Properties_s {
        uint8_t index;
        string model;
        uint64_t cylinders;
        uint32_t tracksPerCylinder;
        uint32_t sectorsPerTrack;
        uint32_t bytesPerSector;
        uint64_t lbaQty;
    };

private:
    list<Properties_s> propertiesList;

public:
    DriveList();
    ~DriveList();
    list<Properties_s> GetPropertiesList();
    string GetModel(uint8_t index);
    uint8_t GetIndex(string &driveModel);
    uint64_t GetLbaQty(string &driveModel);
    void ShowDriveList();

private:
    static bool CheckInIgnoreFile(string &driveModel);
};
