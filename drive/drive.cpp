//---------------------------------------------------------------------------

#include "drive.hpp"
#include "ntddscsi.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define DBG             dbgPrintf
#define DBG_DRIVE       dbgPrintf(" drive: "); dbgPrintf

// --------------------------------------------------------------------------------------------------------------------

static inline void SwapBytes(void *buf, size_t bufSize)
{
    uint8_t *dataBuf = (uint8_t*)buf;
    for (size_t i = 0; i < (bufSize) && (bufSize - i >= 2); i += 2) {
        uint8_t swapVal = dataBuf[i];
        dataBuf[i] = dataBuf[i + 1];
        dataBuf[i + 1] = swapVal;
    }
}

// --------------------------------------------------------------------------------------------------------------------

Drive::Drive(uint8_t driveIndex)
{
    driveHandle         = NULL;
    threadHandle        = NULL;
    index               = driveIndex;
    memset(driveSysName, 0, sizeof(driveSysName));
    processedSize       = 0;
    sliceProcessedSize  = 0;
    sliceTimeMs         = 0;
    checkFailSize       = 0;
    findTargetDataFlag  = false;
    targetDataSector    = 0;
    startTimeMs         = 0;
    activeFlag          = false;
    doneTrigger         = false;
    memset(&overLappedCtx, 0, sizeof(overLappedCtx));
    dbgPrintf           = printf;

    memset(&accessParam, 0, sizeof(AccessParam_s));
    memset(targetData, 0, kSectorSize);
    sprintf(driveSysName, "\\\\.\\PHYSICALDRIVE%u", index);

    driveHandle = CreateFile(   driveSysName, 
                                DRIVE_ACCESS_MODE, 
                                DRIVE_ACCESS_SHARE,
                                NULL, 
                                OPEN_EXISTING, 
                                FILE_FLAG_OVERLAPPED, // | FILE_FLAG_NO_BUFFERING | FILE_FLAG_WRITE_THROUGH, 
                                NULL);

    DBG_DRIVE("create drive %u - %s\n", driveIndex, (driveHandle != INVALID_HANDLE_VALUE) ? "OK" : "FAIL");
}

Drive::~Drive()
{
    DWORD dwInfo;
    GetOverlappedResult(driveHandle, &overLappedCtx, &dwInfo, TRUE);
    
    if(threadHandle != NULL)
    {
        SuspendThread(threadHandle);
        CloseHandle(threadHandle);
    }
    DBG_DRIVE("close drive %u\n", index);
}

void Drive::SetDebugFunc(int (*func) (const char *str, ...))
{
    dbgPrintf = func;
}

bool Drive::IsOpened()
{
    return (driveHandle != INVALID_HANDLE_VALUE) && (driveHandle != NULL);
}

// --------------------------------------------------------------------------------------------------------------------

void Drive::Read(uint64_t lba, uint64_t lbaQty, uint8_t *reqBuf, bool checkRawLba)
{
    static bool checkFailFlag = false;

    // TODO: make true overlapped
    DWORD dwInfo;
    overLappedCtx.Offset = (lba * (uint64_t)kSectorSize) & 0xFFFFFFFF;
    overLappedCtx.OffsetHigh = (lba * (uint64_t)kSectorSize) >> 32;
    ReadFile(driveHandle, reqBuf, lbaQty * kSectorSize, &dwInfo, &overLappedCtx);
    GetOverlappedResult(driveHandle, &overLappedCtx, &dwInfo, TRUE);

    if (checkRawLba)
    {
        for (uint32_t lbaOffset = 0; lbaOffset < lbaQty; lbaOffset++)
        {
            char *markBuf = (char*)&reqBuf[16 + (lbaOffset * kSectorSize)];
            uint64_t markLba = *((uint64_t*)markBuf);

            if ((lba + lbaOffset) != markLba)
            {
                checkFailSize += kSectorSize;

                if (!checkFailFlag)
                {
                    DBG_DRIVE("start of fail-range at lba %llu != ", lba + lbaOffset);
                    DBG("%llu\n", markLba);
                    checkFailFlag = true;
                }
            }
            else if (checkFailFlag)
            {
                DBG_DRIVE("end   of fail-range at lba %llu == ", lba + lbaOffset);
                DBG("%llu\n", markLba);
                checkFailFlag = false;
            }
        }
    }
}

void Drive::Write(uint64_t lba, uint64_t lbaQty, uint8_t *reqBuf, bool markRawLba, bool markStrLba, bool markTime)
{    
    DWORD dwInfo;
    GetOverlappedResult(driveHandle, &overLappedCtx, &dwInfo, TRUE);

    for (uint32_t lbaOffset = 0; lbaOffset < lbaQty; lbaOffset++)
    {
        char *markBuf = (char*)&reqBuf[16 + (lbaOffset * kSectorSize)];
        if (markRawLba)
        {
            *((uint64_t*)&markBuf[0]) = lba + lbaOffset;
        }
        if (markStrLba)
        {
            sprintf(&markBuf[16], "%016llu", lba + lbaOffset);
        }
        if (markTime) 
        {
            time_t now = time(NULL);
            struct tm *current_time = localtime(&now);
            strftime(&markBuf[32], 16, "%Y-%m-%d", current_time);
            strftime(&markBuf[48], 16, "%H:%M:%S", current_time);
        }
    }
    
    overLappedCtx.Offset = (lba * (uint64_t)kSectorSize) & 0xFFFFFFFF;
    overLappedCtx.OffsetHigh = (lba * (uint64_t)kSectorSize) >> 32; 
    WriteFile(driveHandle, reqBuf, lbaQty * kSectorSize, &dwInfo, &overLappedCtx);
}

void Drive::ReadLoad()
{
    ResetCounters();

    uint8_t * const reqBuf = (uint8_t*)malloc(accessParam.reqBufSize);

    for(uint32_t itrInd = 0; itrInd < accessParam.itrQty; itrInd++)
    {
        if(doneTrigger) {break;}

        const uint32_t lbaQtyPerReq = accessParam.reqBufSize / kSectorSize;

	    for(uint64_t lbaInd = 0; lbaInd < accessParam.lbaQty; lbaInd += lbaQtyPerReq)
	    {
            if(doneTrigger) {break;}

            uint64_t lba = accessParam.startLba + lbaInd;

            if(accessParam.randEn)
            {
                lba = accessParam.startLba + GetRandomLbaOffset(lbaInd);
            }

            Read(lba, lbaQtyPerReq, reqBuf, accessParam.checkRawLba);

            if(findTargetDataFlag)
            {
                for(uintptr_t offset = 0; offset < accessParam.reqBufSize; offset += kSectorSize)
                {
                    bool cmpRes = true;

                    for(uintptr_t k = 0; (k < kSectorSize) && cmpRes; k++)
                    {
                        cmpRes = cmpRes && (reqBuf[offset + k] == targetData[k]);
                    }

                    if(cmpRes)
                    {
                        targetDataSector = lba + (offset / kSectorSize);
                        DBG_DRIVE("Find sector %u with target data", targetDataSector);
                    }
                }
            }

		    processedSize += accessParam.reqBufSize;
            accessParam.reqPattern = ((uint32_t*)reqBuf)[4];
        }
	}

    activeFlag = false;
    doneTrigger = false;
    free(reqBuf);
}

void Drive::WriteLoad()
{
    ResetCounters();
    uint64_t operationCnt = 0;

    uint8_t * reqBufArr[2];
    reqBufArr[0] = (uint8_t*)malloc(accessParam.reqBufSize);
    reqBufArr[1] = (uint8_t*)malloc(accessParam.reqBufSize);

    for(uint32_t i = 0; i < accessParam.reqBufSize / sizeof(uint32_t); i++)
    {
        ((uint32_t*)reqBufArr[0])[i] = accessParam.reqPattern;
        ((uint32_t*)reqBufArr[1])[i] = accessParam.reqPattern;
    }

    for(uint32_t itrInd = 0; itrInd < accessParam.itrQty; itrInd++)
    {
        if(doneTrigger) {break;}

        const uint32_t lbaQtyPerReq = accessParam.reqBufSize / kSectorSize;

	    for(uint64_t lbaInd = 0; lbaInd < accessParam.lbaQty; lbaInd += lbaQtyPerReq)
	    {
            if(doneTrigger) {break;}

            uint64_t lba = accessParam.startLba + lbaInd;

            if(accessParam.randEn)
            {
                lba = accessParam.startLba + GetRandomLbaOffset(lbaInd);
            }

            Write(lba, lbaQtyPerReq, reqBufArr[operationCnt % 2], accessParam.markRawLba, accessParam.markStrLba, accessParam.markTime);

            operationCnt++;
		    processedSize += accessParam.reqBufSize;
        }
	}

    activeFlag = false;
    doneTrigger = false;
    free(reqBufArr[0]);
    free(reqBufArr[1]);
}

void Drive::Trim(uint64_t startLba, uint64_t lbaQty)
{
    DBG_DRIVE("start trim drive %u\n", index);
    const uint64_t stopLba = startLba + lbaQty;

    const uint16_t lbaQtyPerRange   = 0xFFFF;
    const uint16_t rangesPerSect    = kSectorSize / sizeof(uint64_t);
    const uint32_t sendSectQty      = 1 + (uint32_t)((lbaQty / lbaQtyPerRange) / rangesPerSect); //TODO: change +1 to round up

    uint64_t * const trimData = (uint64_t*)malloc(kSectorSize);
    uint64_t curLba = startLba;
    
    for (uint32_t sendSectInd = 0; sendSectInd < sendSectQty; sendSectInd++)
    {
        for (uint32_t trimRangeInd = 0; trimRangeInd < rangesPerSect; trimRangeInd++)
        {
            trimData[trimRangeInd] = 0;

            if (stopLba > curLba)
            {
                trimData[trimRangeInd] = curLba;
                uint64_t lbaQtyStep = ((stopLba - curLba) > lbaQtyPerRange) ? lbaQtyPerRange : (stopLba - curLba);
                trimData[trimRangeInd] |= lbaQtyStep << 48;
                curLba += lbaQtyStep;
            }
        }
        DBG_DRIVE("send trim buffer %u/%u\n", sendSectInd + 1, sendSectQty);
        TrimSectorBuf(trimData);
    }

    free(trimData);
}

void Drive::Clean()
{
    DBG_DRIVE("start clean drive %u\n", index);
    const uint32_t kCleanLbaQty = 1024;
    accessParam.startLba    = 0;
    accessParam.lbaRange    = kCleanLbaQty;
    accessParam.lbaQty      = kCleanLbaQty;
    accessParam.itrQty      = 1;
    accessParam.reqBufSize  = kCleanLbaQty * kSectorSize;
    accessParam.reqPattern  = 0x00000000;
    accessParam.randEn      = false;
    accessParam.dirWrite    = true;
    WriteLoad();
    system("echo rescan | diskpart");
}

// --------------------------------------------------------------------------------------------------------------------

void Drive::SetAccessParam(AccessParam_s &newAccessParam)
{
    accessParam = newAccessParam;
}

Drive::AccessParam_s Drive::GetAccessParam()
{
    return accessParam;
}

DWORD WINAPI Drive::TestThread(LPVOID lpParam)
{
    Drive *hDrive = (Drive*)lpParam;
    if(hDrive->GetAccessParam().dirWrite)
    {
        hDrive->WriteLoad();
    }
    else
    {
        hDrive->ReadLoad();
    }
    return 0;
}

void Drive::StartTest()
{
    DBG_DRIVE("start test drive %u %s\n", index, accessParam.dirWrite ? "write" : "read");
    activeFlag = true;
    ResetCounters();
    threadHandle = CreateThread(NULL, 0, Drive::TestThread, (void*)this, 0, NULL);
    //SetPriorityClass(hThread, REALTIME_PRIORITY_CLASS);
    SetThreadPriority(threadHandle, THREAD_PRIORITY_TIME_CRITICAL);
}

void Drive::SuspendTest()
{
    if(threadHandle != INVALID_HANDLE_VALUE)
    {
        DBG_DRIVE("suspend test drive %u\n", index);
        SuspendThread(threadHandle);
    }
}

void Drive::ResumeTest()
{
    if(threadHandle != INVALID_HANDLE_VALUE)
    {
        DBG_DRIVE("resume test drive %u\n", index);
        ResumeThread(threadHandle);
    }
}

void Drive::StopTest()
{
    doneTrigger = true;
    ResumeTest();
    while (activeFlag) {}
    DBG_DRIVE("stop test drive %u\n", index);
}

bool Drive::IsTestDone()
{
    return !activeFlag;
}

// --------------------------------------------------------------------------------------------------------------------

uint64_t Drive::GetTimeLeftMs()
{
    uint64_t curTimeMs = clock() / (1000 / CLOCKS_PER_SEC);
    return curTimeMs - startTimeMs; 
}

uint64_t Drive::GetProcessedSize()
{
    return processedSize;
}

uint64_t Drive::GetSpeedSlice()
{
    const uint64_t timeLeftMs = GetTimeLeftMs();
    const uint64_t lastProcessedSize = processedSize - sliceProcessedSize;

    uint64_t lastTimeLeftMs = timeLeftMs - sliceTimeMs;
    lastTimeLeftMs = (lastTimeLeftMs == 0) ? 1 : lastTimeLeftMs;

    sliceProcessedSize = processedSize;
    sliceTimeMs = timeLeftMs;

    uint64_t sliceSpeedBps = (lastProcessedSize * 1000) / lastTimeLeftMs;

    return sliceSpeedBps;
}

uint64_t Drive::GetCheckFailSize()
{
    return checkFailSize;
}

uint32_t Drive::GetLastData()
{
    return accessParam.reqPattern;
}

void Drive::SetTargetData(uint8_t *targetDataBuf)
{
    findTargetDataFlag = true;
    memcpy(targetData, targetDataBuf, kSectorSize);
}

uint64_t Drive::GetTargetSector()
{
    return targetDataSector;
}

// --------------------------------------------------------------------------------------------------------------------

bool Drive::GetIdentify(IDENTIFY_DEVICE_DATA *identifyBuf)
{
    const uint16_t headerSize = sizeof(SCSI_PASS_THROUGH_DIRECT);
    const uint16_t senseSize = 32;

    SCSI_PASS_THROUGH_DIRECT scsiData;
    memset(&scsiData, 0, sizeof(scsiData));
    uint8_t senseBuf[headerSize + senseSize];
    memset(senseBuf, 0, sizeof(senseBuf));
    uint8_t dataBuf[kSectorSize];
    memset(dataBuf, 0, sizeof(dataBuf));

    scsiData.Length = headerSize;
    scsiData.CdbLength = 16;
    scsiData.DataIn = SCSI_IOCTL_DATA_IN;
    scsiData.DataTransferLength = kSectorSize;
    scsiData.TimeOutValue = 20;
    scsiData.DataBuffer = dataBuf;
    scsiData.SenseInfoLength = senseSize;
    scsiData.SenseInfoOffset = headerSize;
    scsiData.Cdb[0] = 0x85;
    scsiData.Cdb[1] = 0x7;
    scsiData.Cdb[2] = 0x24;
    scsiData.Cdb[14] = 0xEC;

    DWORD dwReturn = 0;

    bool status = DeviceIoControl(driveHandle,
            IOCTL_SCSI_PASS_THROUGH_DIRECT, 
            &scsiData, sizeof(scsiData),
            senseBuf, sizeof(senseBuf),
            &dwReturn, NULL); 

    memcpy(identifyBuf, dataBuf, sizeof(IDENTIFY_DEVICE_DATA));
    SwapBytes(identifyBuf, sizeof(IDENTIFY_DEVICE_DATA));

    DBG("Idn\n");
    DBG_DRIVE("serial:    %.*s\n", (int)sizeof(identifyBuf->SerialNumber), identifyBuf->SerialNumber);
    DBG_DRIVE("firmware:  %.*s\n", (int)sizeof(identifyBuf->FirmwareRevision), identifyBuf->FirmwareRevision);
    DBG_DRIVE("model:     %.*s\n", (int)sizeof(identifyBuf->ModelNumber), identifyBuf->ModelNumber);
    //DBG_DRIVE("Raw data:\n");
    //ShowBuf(identifyBuf, sizeof(IDENTIFY_DEVICE_DATA));
    return status;
}

bool Drive::SendAtaCommand(uint8_t cmd, uint16_t feature, uint64_t lba)
{
    ATA_PASS_THROUGH_EX ataData;
    memset(&ataData, 0, sizeof(ataData));

    ataData.Length = sizeof(ataData);
    ataData.DataBufferOffset = 0;
    ataData.DataTransferLength = 0;
    ataData.AtaFlags = ATA_FLAGS_48BIT_COMMAND;
    ataData.TimeOutValue = 10;

    ataData.PreviousTaskFile[0] = 0;
    ataData.PreviousTaskFile[1] = 0;
    ataData.PreviousTaskFile[2] = (lba >> 24) & 0xFF;
    ataData.PreviousTaskFile[3] = (lba >> 32) & 0xFF;
    ataData.PreviousTaskFile[4] = (lba >> 40) & 0xFF;
    ataData.PreviousTaskFile[5] = 0xE0;
    ataData.PreviousTaskFile[6] = 0;
    ataData.PreviousTaskFile[7] = 0;

    ataData.CurrentTaskFile[0] = feature;
    ataData.CurrentTaskFile[1] = 1;
    ataData.CurrentTaskFile[2] = (lba >> 0) & 0xFF;
    ataData.CurrentTaskFile[3] = (lba >> 8) & 0xFF;
    ataData.CurrentTaskFile[4] = (lba >> 16) & 0xFF;
    ataData.CurrentTaskFile[5] = 0xE0;
    ataData.CurrentTaskFile[6] = cmd;
    ataData.CurrentTaskFile[7] = 0;

    DWORD dwReturn = 0;

    bool status = DeviceIoControl(driveHandle,
            IOCTL_ATA_PASS_THROUGH_DIRECT, 
            &ataData, sizeof(ataData),
            NULL, 0,
            &dwReturn, NULL); 

    DBG_DRIVE("ata cmd %02X status %u\n", cmd, status);
    return status;
}

bool Drive::SendScsiCommand(uint8_t cmd, uint16_t feature, uint64_t lba)
{
    const uint16_t headerSize = sizeof(SCSI_PASS_THROUGH_DIRECT);
    const uint16_t senseSize = 32;

    SCSI_PASS_THROUGH_DIRECT scsiData;
    memset(&scsiData, 0, sizeof(scsiData));
    uint8_t senseBuf[headerSize + senseSize];
    memset(senseBuf, 0, sizeof(senseBuf));
    
    scsiData.Length = headerSize;
    scsiData.CdbLength = 16;
    scsiData.DataIn = SCSI_IOCTL_DATA_OUT;
    scsiData.DataTransferLength = 0;
    scsiData.TimeOutValue = 20;
    scsiData.DataBuffer = NULL;
    scsiData.SenseInfoLength = senseSize;
    scsiData.SenseInfoOffset = headerSize;

    scsiData.Cdb[0] = 0x85;
    scsiData.Cdb[1] = 0x7;
    scsiData.Cdb[2] = 0x24;
    scsiData.Cdb[3] = 0x0;
    scsiData.Cdb[4] = feature;
    scsiData.Cdb[5] = 0x0;
    scsiData.Cdb[6] = 0x0;
    scsiData.Cdb[7] = (lba >> 24) & 0xFF;
    scsiData.Cdb[8] = (lba >> 0) & 0xFF;
    scsiData.Cdb[9] = 0x0;
    scsiData.Cdb[10] = (lba >> 8) & 0xFF;
    scsiData.Cdb[11] = 0x0;
    scsiData.Cdb[12] = (lba >> 16) & 0xFF;
    scsiData.Cdb[13] = 0x0;
    scsiData.Cdb[14] = cmd;
    scsiData.Cdb[15] = 0x0;

    DWORD dwReturn = 0;

    bool status = DeviceIoControl(driveHandle,
            IOCTL_SCSI_PASS_THROUGH_DIRECT, 
            &scsiData, sizeof(scsiData),
            senseBuf, sizeof(senseBuf),
            &dwReturn, NULL); 

    DBG_DRIVE("scsi cmd %02X status %u\n", cmd, status);

    uint8_t *sense = (uint8_t*)&senseBuf[headerSize];
    uint16_t progressRaw = ((uint16_t)sense[17] << 8) | sense[15];
    uint8_t progressPerCent = (uint32_t)100 * progressRaw / 0xFFFF;
    DBG_DRIVE("progress %u%% (%u)\n", progressPerCent, progressRaw);
    return status;
}

void Drive::GetSmart(smartAttribute_s *outSmartAttributes)
{
    SENDCMDINPARAMS sataRequest = {};

    sataRequest.cBufferSize                     = READ_ATTRIBUTE_BUFFER_SIZE;
    sataRequest.bDriveNumber                    = index;
    sataRequest.irDriveRegs.bCommandReg         = SMART_CMD;
    sataRequest.irDriveRegs.bCylLowReg          = SMART_CYL_LOW;
    sataRequest.irDriveRegs.bCylHighReg         = SMART_CYL_HI;
    sataRequest.irDriveRegs.bSectorCountReg     = 1;
    sataRequest.irDriveRegs.bSectorNumberReg    = 1;
    sataRequest.irDriveRegs.bDriveHeadReg       = 0xA0; // | (1 << 4);
    sataRequest.irDriveRegs.bFeaturesReg        = READ_ATTRIBUTES;

    DWORD dwReturn = 0;
    const uint32_t kRawBufSize = (sizeof(SENDCMDOUTPARAMS) + READ_ATTRIBUTE_BUFFER_SIZE - 1);
    BYTE outBuf[kRawBufSize] = {0};
    SENDCMDOUTPARAMS *sataRawAttributes = (SENDCMDOUTPARAMS*)outBuf;

    DeviceIoControl(driveHandle, SMART_RCV_DRIVE_DATA, &sataRequest, sizeof(sataRequest),
                    sataRawAttributes, kRawBufSize, &dwReturn, NULL);

    smartBuf_s *smartBuf = (smartBuf_s*)sataRawAttributes->bBuffer;
    smartAttribute_s *smartAttribute = (smartAttribute_s*)smartBuf->Data;

    for(uint8_t attrInd = 0; attrInd < SMART_ATTR_MAX_ID; attrInd++)
    {
        memcpy(&outSmartAttributes[attrInd], &smartAttribute[attrInd], sizeof(smartAttribute_s));

        if(smartAttribute[attrInd].Id == SMART_ATTR_INVALID_ID)
        {
            break;
        }
    }
}

int8_t Drive::GetTemperature()
{
    int8_t resTemperature = 127;
    smartAttribute_s smartAttributes[SMART_ATTR_MAX_ID];
    GetSmart(smartAttributes);

    for(uint8_t attrInd = 0; attrInd < SMART_ATTR_MAX_ID; attrInd++)
    {
        if(smartAttributes[attrInd].Id == SMART_ATTR_THERMAL_ID)
        {
            resTemperature = smartAttributes[attrInd].RawValue[0];
            break;
        }
    }

    return resTemperature;
}

// Private ------------------------------------------------------------------------------------------------------------

uint64_t Drive::GetRandomLbaOffset(uint64_t lbaInd)
{
    (void)lbaInd;
    uint64_t randVal = 0;
    randVal |= (uint64_t)rand() << 49; 
    randVal |= (uint64_t)rand() << 33; 
    randVal |= (uint64_t)rand() << 17; 
    randVal |= (uint64_t)rand() << 1; 
    randVal |= (uint64_t)rand() & 0x0001; 

    const uint32_t lbaQtyPerReq = accessParam.reqBufSize / kSectorSize;
    const uint64_t lbaOffset = randVal % accessParam.lbaRange;
    const uint64_t alignLbaOffset = (lbaOffset / lbaQtyPerReq) * lbaQtyPerReq;

    return alignLbaOffset;
}

void Drive::TrimSectorBuf(void *trimDataBuf)
{
    const uint16_t headerSize   = sizeof(ATA_PASS_THROUGH_EX);
    const uint8_t sendSectQty   = 1;
    const uint16_t trimDataSize = kSectorSize * sendSectQty;
    const uint32_t cmdBufSize   = headerSize + trimDataSize;

    uint8_t * const cmdBuf = (uint8_t*)malloc(cmdBufSize);
    memset(cmdBuf, 0, cmdBufSize);

    ATA_PASS_THROUGH_EX *command = (ATA_PASS_THROUGH_EX*)cmdBuf;

    command->Length = headerSize;
    command->AtaFlags = ATA_FLAGS_DATA_OUT;
    command->TimeOutValue = 1;
    command->DataBufferOffset = headerSize;
    command->DataTransferLength = trimDataSize;

    command->CurrentTaskFile[0] = 0x01;
    command->CurrentTaskFile[1] = sendSectQty;
    command->CurrentTaskFile[2] = 0;
    command->CurrentTaskFile[3] = 0;
    command->CurrentTaskFile[4] = 0;
    command->CurrentTaskFile[5] = 0;
    command->CurrentTaskFile[6] = 0x06;
    command->CurrentTaskFile[7] = 0;

    uint8_t *trimData = (uint8_t*)&(cmdBuf[headerSize]);
    memcpy(trimData, trimDataBuf, trimDataSize);

    DWORD dwReturn = 0;

    DeviceIoControl(driveHandle, IOCTL_ATA_PASS_THROUGH, cmdBuf, cmdBufSize, NULL, 0, &dwReturn, NULL);

    free(cmdBuf);
}

void Drive::ResetCounters()
{
    processedSize = 0;
    sliceProcessedSize = 0;
    sliceTimeMs = 0;
    checkFailSize = 0;
    startTimeMs = clock() / (1000 / CLOCKS_PER_SEC);
}
