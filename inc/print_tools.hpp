#pragma once

#include <stdio.h>
#include <stdint.h>

#define MIN_FILL_QTY(size, fillSize)        (((size) / (fillSize)) + (((size) % (fillSize)) == 0 ? 0 : 1))

static inline void ShowBuf(void *buf, size_t bufSize, int (*prtf) (const char *str, ...))
{
    const uint8_t *dataBuf = (uint8_t*)buf;
    const uint32_t kBytesPerLine = 16;
    const uint32_t kBytesPerBlock = 128;

    for (uint32_t line = 0; line < MIN_FILL_QTY(bufSize, kBytesPerLine); line++) {
        prtf("%08X | ", line * kBytesPerLine);

        // Print as HEX
        for (uint32_t i = 0; i < kBytesPerLine; i++) {
            uint32_t byteInd = line * kBytesPerLine + i;
            if (byteInd < bufSize) {
                prtf("%02X ", dataBuf[byteInd]);
            } else {
                prtf("   ");
            }
            if (i == ((kBytesPerLine / 2) - 1)) {
                prtf(" ");
            }
        }
        prtf("| ");

        // print as ASCII
        for (uint32_t i = 0; i < kBytesPerLine; i++) {
            uint32_t byteInd = line * kBytesPerLine + i;
            if (byteInd < bufSize) {
                prtf("%c", (dataBuf[byteInd] < 32) || (dataBuf[byteInd] > 127) ? '.' : dataBuf[byteInd]);
            } else {
                prtf(" ");
            }
        }

        if ((line + 1) % (kBytesPerBlock / kBytesPerLine) == 0) {
            prtf("\n");
        }
        prtf("\n");
    }
    prtf("\n");
}

static inline void ShowSpeed(uint64_t speedBytesPerSec, int (*prtf) (const char *str, ...))
{
    if (speedBytesPerSec < 1000) {
        prtf("%llu B/s", speedBytesPerSec);
    } 
    else if (speedBytesPerSec < 1000 * 1000) {
        prtf("%.2f KB/s", (float)speedBytesPerSec / 1000);
    } 
    else if (speedBytesPerSec < 1000 * 1000 * 1000) {
        prtf("%.2f MB/s", (float)speedBytesPerSec / (1000 * 1000));
    } 
    else {
        prtf("%.2f GB/s", (float)speedBytesPerSec / (1000 * 1000 * 1000));
    }
}

static inline void ShowIops(uint64_t ioPerSec, int (*prtf) (const char *str, ...))
{
    if (ioPerSec < 1000) {
        prtf("%llu IOps", ioPerSec);
    } 
    else if (ioPerSec < 1000 * 1000) {
        prtf("%.2f KIOps", (float)ioPerSec / 1000);
    } 
    else if (ioPerSec < 1000 * 1000 * 1000) {
        prtf("%.2f MIOps", (float)ioPerSec / (1000 * 1000));
    } 
    else {
        prtf("%.2f GIOps", (float)ioPerSec / (1000 * 1000 * 1000));
    }
}

static inline void ShowSize(uint64_t sizeBytes, int (*prtf) (const char *str, ...))
{
    if (sizeBytes < 1000) {
        prtf("%llu B", sizeBytes);
    } 
    else if (sizeBytes < 1000 * 1000) {
        prtf("%.2f KB", (float)sizeBytes / 1024);
    } 
    else if (sizeBytes < 1000 * 1000 * 1000) {
        prtf("%.2f MB", (float)sizeBytes / (1024 * 1024));
    } 
    else {
        prtf("%.2f GB", (float)sizeBytes / (1024 * 1024 * 1024));
    }
}

