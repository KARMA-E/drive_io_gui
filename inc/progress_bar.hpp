#pragma once
#include <iostream>
using namespace std;

template<typename Type>
class ProgressBar
{
private:
    int pos = 0;
    int len = 32;
    Type begin;
    Type end;
    Type range;

public:
    ProgressBar(Type beginVal, Type endVal)
    {
        begin = beginVal;
        end = endVal;
        range = end - begin;
    }

    ProgressBar(Type beginVal, Type endVal, int barLen)
    {
        len = barLen;
        begin = beginVal;
        end = endVal;
        range = end - begin;

        cout << "[";

        for (int i = 0; i <= len; i++){
            cout << "-";
        }

        cout << "\b]";

        for (int i = 0; i <= len; i++){
            cout << "\b";
        }
    }

    ~ProgressBar() 
    {
        int diff = len - pos;

        for (int i = 0; i < diff; i++)
        {
            cout << "-";
        }
        cout << "]";
    }

    void Push(Type value)
    {
        Type temp = value - begin;
        int newPos = len * ((double)temp / range);
        newPos = newPos < len ? newPos : len;
        int diff = newPos - pos;

        for (int i = 0; i < abs(diff); i++)
        {
            if (diff > 0){
                cout << "#";
            }
            else{
                cout << "\b-\b";
            }
        }

        pos = newPos;
    }
};
