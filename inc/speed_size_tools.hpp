#pragma once

#include <stdio.h>
#include <stdint.h>

static inline void ShowSpeed(uint64_t speedBytesPerSec)
{
    if (speedBytesPerSec < 1000) {
        printf("%llu B/s", speedBytesPerSec);
    } 
    else if (speedBytesPerSec < 1000 * 1000) {
        printf("%.2f KB/s", (float)speedBytesPerSec / 1000);
    } 
    else if (speedBytesPerSec < 1000 * 1000 * 1000) {
        printf("%.2f MB/s", (float)speedBytesPerSec / (1000 * 1000));
    } 
    else {
        printf("%.2f GB/s", (float)speedBytesPerSec / (1000 * 1000 * 1000));
    }
}

static inline void ShowIops(uint64_t ioPerSec)
{
    if (ioPerSec < 1000) {
        printf("%llu IOps", ioPerSec);
    } 
    else if (ioPerSec < 1000 * 1000) {
        printf("%.2f KIOps", (float)ioPerSec / 1000);
    } 
    else if (ioPerSec < 1000 * 1000 * 1000) {
        printf("%.2f MIOps", (float)ioPerSec / (1000 * 1000));
    } 
    else {
        printf("%.2f GIOps", (float)ioPerSec / (1000 * 1000 * 1000));
    }
}

static inline void ShowSize(uint64_t sizeBytes)
{
    if (sizeBytes < 1000) {
        printf("%llu B", sizeBytes);
    } 
    else if (sizeBytes < 1000 * 1000) {
        printf("%.2f KB", (float)sizeBytes / 1024);
    } 
    else if (sizeBytes < 1000 * 1000 * 1000) {
        printf("%.2f MB", (float)sizeBytes / (1024 * 1024));
    } 
    else {
        printf("%.2f GB", (float)sizeBytes / (1024 * 1024 * 1024));
    }
}