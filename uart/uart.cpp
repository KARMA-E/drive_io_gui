#include "uart.hpp"
#include <stdio.h>

enum
{
    PARSE_SEARCH_BEGIN,
    PARSE_SEARCH_END,
    PARSE_PARSING
};
typedef uint8_t ParseState_t;

DWORD WINAPI UartReadThread(LPVOID param)
{
    Uart::UartBuffer *curUartBuffer = (Uart::UartBuffer*)param;
    ParseState_t parseState = PARSE_SEARCH_BEGIN;

    char dataBuf[4096];

    uint16_t beginDataInd = 0;
    uint16_t endDataInd = 0;
    uint16_t curDataInd = 0;
    uint16_t dataBufSize = sizeof(dataBuf);

	while(1)
	{
        DWORD rdByteQty;

        //TODO: change size
        ReadFile(curUartBuffer->comPortHandle, &dataBuf[curDataInd], 4, &rdByteQty, NULL);

        while(rdByteQty != 0)
        {
            if(parseState == PARSE_SEARCH_BEGIN)
            {
                if(dataBuf[curDataInd] == '\n')
                {
                    beginDataInd = curDataInd + 1;
                    parseState = PARSE_SEARCH_END;
                }
            }
            else if(parseState == PARSE_SEARCH_END)
            {
                if(dataBuf[curDataInd] == '\3')
                {
                    endDataInd = curDataInd;
                    parseState = PARSE_PARSING;
                }
            }

            rdByteQty--;
            curDataInd++;

            if(parseState == PARSE_PARSING)
            {
                char tempBuf[4096];
                uint16_t tempBufInd = 0;
                uint16_t valArrInd = 0;

                for(uint16_t i = beginDataInd; i <= endDataInd; i++)
                {
                    char data = dataBuf[i];
                    if(data != ' ' && i < endDataInd)
                    {
                        tempBuf[tempBufInd] = data;
                        tempBufInd++;
                    }
                    else
                    {
                        tempBuf[tempBufInd] = '\0';
                        tempBufInd = 0;
                        curUartBuffer->valTable[valArrInd] = atoi(tempBuf);
                        valArrInd++;
                    }
                }

                if(curDataInd > (dataBufSize / 2))
                {
                    printf("uart: switch uartDataBuf to zero index\r\n");
                    
                    for(uint16_t i = 0; i < (uint16_t)rdByteQty; i++)
                    {
                        dataBuf[i] = dataBuf[curDataInd + i];
                    }
                    curDataInd = 0;
                }
                parseState = PARSE_SEARCH_BEGIN;
            }
        }
	}
}

Uart::Uart(char *comPortName, uint32_t baudrate, uint8_t parity, uint8_t stopBits)
{
    strcpy(__comPortName, comPortName);

    char comPortSysName[32];
    sprintf(comPortSysName, "\\\\.\\%s", comPortName);

    __uartBuffer.comPortHandle = CreateFile(comPortSysName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

    if(IsOpen())
    {
        DCB dcbSerialParams = {};
        dcbSerialParams.DCBlength=sizeof(dcbSerialParams);

        GetCommState(__uartBuffer.comPortHandle, &dcbSerialParams);

        dcbSerialParams.BaudRate = baudrate;
        dcbSerialParams.fBinary = true;
        dcbSerialParams.fParity = false;
        dcbSerialParams.fOutxCtsFlow = false;
        dcbSerialParams.fOutxDsrFlow = false;
        dcbSerialParams.fDtrControl = DTR_CONTROL_DISABLE;
        dcbSerialParams.fDsrSensitivity = false;
        dcbSerialParams.fTXContinueOnXoff = false;
        dcbSerialParams.fOutX = false;
        dcbSerialParams.fInX = false;
        dcbSerialParams.fNull = false;
        dcbSerialParams.fRtsControl = RTS_CONTROL_DISABLE;
        dcbSerialParams.fAbortOnError = false;
        dcbSerialParams.ByteSize = 8;
        dcbSerialParams.Parity = parity;
        dcbSerialParams.StopBits = stopBits;

        SetCommState(__uartBuffer.comPortHandle, &dcbSerialParams);
        SetupComm(__uartBuffer.comPortHandle,2000,2000);
        PurgeComm(__uartBuffer.comPortHandle, PURGE_RXCLEAR);

        __readThreadHandle = CreateThread(NULL, 0, UartReadThread, (void*)&__uartBuffer, 0, NULL);

        printf("uart: open %s %u %u and start thread\r\n", comPortSysName, baudrate, parity);
    }
    else
    {
        printf("uart: can't open %s\r\n", comPortSysName);
    }
}

Uart::~Uart()
{
    if (IsOpen())
    {
        TerminateThread(__readThreadHandle, 0);
        CloseHandle(__readThreadHandle);
        CloseHandle(__uartBuffer.comPortHandle);
        //__comPortHandle = INVALID_HANDLE_VALUE;
    }
    
    printf("uart: call distructor %llu\r\n", (uint64_t)__uartBuffer.comPortHandle);
}

bool Uart::IsOpen()
{
    return __uartBuffer.comPortHandle != INVALID_HANDLE_VALUE;
}

int32_t Uart::GetVal(uint8_t valInd)
{
    if(valInd < kValTableLen)
    {
        return __uartBuffer.valTable[valInd];
    }
    
    return 0;
}

