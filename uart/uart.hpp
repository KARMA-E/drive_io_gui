#pragma once

#include <stdint.h>
#include <windows.h>

class Uart
{
public:
    enum
    {
        kComPortNameSize = 32,
        kValTableLen = 64,
    };
    
private:
    friend DWORD WINAPI UartReadThread(LPVOID param);

    struct UartBuffer
    {
        HANDLE comPortHandle;
        uint32_t valTable[kValTableLen];
    } __uartBuffer;

    HANDLE __readThreadHandle;
    char __comPortName[kComPortNameSize];

public:
    Uart(char *comPortName, uint32_t baudrate, uint8_t parity, uint8_t stopBits);
    ~Uart();

    bool IsOpen();
    void PutInBuf(char *rawBuf, uint32_t rawBufSize);
    int32_t GetVal(uint8_t valInd);
};

