//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#pragma warn -8057

#include "Unit1.h"
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>

#include <algorithm>
using namespace std;

#include "drive.hpp"
#include "drive_list.hpp"
#include "uart.hpp"

#include <io.h>
#include <stdarg.h>
#include <fcntl.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

//---------------------------------------------------------------------------

static Drive *hDrive = NULL;
static DriveList *hDriveList = NULL;
static Uart *hUart = NULL;
static bool _graphFullScrEnable = false;

enum SeriesType_e
{
    SERIES_TYPE_WRITE,
    SERIES_TYPE_READ,
    SERIES_TYPE_TEMPERATURE
};
typedef uint8_t SeriesType_t;

//---------------------------------------------------------------------------

static int _PrintMemo(const char *str, ...)
{
    va_list args;
	va_start(args, str);
	char sendBuf[4096];
	vsprintf((char*)sendBuf, str, args);

    size_t len = strlen(sendBuf);
    size_t lineInd = Form1->Memo1->Lines->Count - 1;
    for (size_t i = 0; i < len; i++)
    {
        if (sendBuf[i] != '\n')
        {
            Form1->Memo1->Lines->Strings[lineInd] = Form1->Memo1->Lines->Strings[lineInd] + sendBuf[i];
        }
        else
        {
            Form1->Memo1->Lines->Add("");
        }
    }
	va_end(args);
    return 0;
}



static void _AddSeriesXY(SeriesType_t seriesType, double x, double y)
{
    float filtCoef = 20;
    TryStrToFloat(Form1->EditGraph1->Text, filtCoef);
    filtCoef = (filtCoef >= 0  ) ? filtCoef : 0;
    filtCoef = (filtCoef <= 100) ? filtCoef : 100;

    if(y == 0)
    {
        y = 0.00000001;
    }

    static double filtRdY = 0;
    static double filtWrY = 0;
    static double filtTpY = 0;

    TPointSeries *curPointSeries;
    TLineSeries *curLineSeries;
    double *curFiltY;

    if(seriesType == SERIES_TYPE_READ)
    {
        curPointSeries = Form1->Series1;
        curLineSeries = Form1->Series2;
        curFiltY = &filtRdY;
    }
    else if(seriesType == SERIES_TYPE_WRITE)
    {
        curPointSeries = Form1->Series3;
        curLineSeries = Form1->Series4;
        curFiltY = &filtWrY;
    }
    else if(seriesType == SERIES_TYPE_TEMPERATURE)
    {
        curPointSeries = Form1->Series5;
        curLineSeries = Form1->Series6;
        curFiltY = &filtTpY;
    }
    else
    {
        return;
    }

    filtCoef    = (*curFiltY != 0) ? filtCoef : 100;
    *curFiltY   = ((100 - filtCoef) * (*curFiltY) + filtCoef * y) / 100;

    curPointSeries->AddXY(x, y);
    curLineSeries->AddXY(x, *curFiltY);

    uint32_t scrollRangeX = StrToInt(Form1->EditGraph2->Text);

    if(Form1->CheckBoxGraph3->Checked)
    {
        if(x > scrollRangeX)
        {
            Form1->Chart1->BottomAxis->Maximum = x;
            Form1->Chart1->BottomAxis->Minimum = x - scrollRangeX;
        }
        else
        {
            Form1->Chart1->BottomAxis->Maximum = scrollRangeX;
            Form1->Chart1->BottomAxis->Minimum = 0;
        }
    }
    else
    {
        Form1->Chart1->BottomAxis->Maximum = x;
        Form1->Chart1->BottomAxis->Minimum = 0;
    }
}

static void _ChartClear()
{
    Form1->Chart1->BottomAxis->Minimum = 0;
    Form1->Chart1->BottomAxis->Maximum = StrToInt(Form1->EditGraph2->Text);
    Form1->Chart1->LeftAxis->Minimum = 0;
    Form1->Chart1->LeftAxis->Maximum = Form1->ScrollBar1->Position;
    Form1->Chart1->RightAxis->Minimum = 0;
    Form1->Chart1->RightAxis->Maximum = Form1->ScrollBar2->Position;
    Form1->Series1->Clear();
    Form1->Series2->Clear();
    Form1->Series3->Clear();
    Form1->Series4->Clear();
    Form1->Series5->Clear();
    Form1->Series6->Clear();
    Form1->Series7->Clear();
    Form1->Series8->Clear();
}

static AnsiString _SecToTime(uint64_t seconds)
{
    tm timeParse;
    timeParse.tm_sec  = (int)(seconds % 60);
    timeParse.tm_min  = (int)(seconds /60) % 60;
    timeParse.tm_hour = (int)(seconds /60) / 60;

    AnsiString res = "_______________________";
    strftime(res.c_str(), strlen(res.c_str()), "%H:%M:%S", &timeParse);
    return res;
}

void _TestEnvEnabled(bool envEnabled)
{
    Form1->ComboBoxDrive1->Enabled = envEnabled;
    Form1->EditAccess1->Enabled = envEnabled;
    Form1->EditAccess2->Enabled = envEnabled;
    Form1->EditAccess3->Enabled = envEnabled;
    Form1->EditAccess4->Enabled = envEnabled;
    Form1->RadioButtonAccess1->Enabled = envEnabled;
    Form1->RadioButtonAccess2->Enabled = envEnabled;
    Form1->RadioButtonAccess3->Enabled = envEnabled;
    Form1->RadioButtonAccess4->Enabled = envEnabled;
    Form1->RadioButtonGraph1->Enabled = envEnabled;
    Form1->RadioButtonGraph2->Enabled = envEnabled;
    Form1->RadioButtonGraph3->Enabled = envEnabled;
    Form1->Button2->Enabled = !envEnabled;
    Form1->Button2->TabStop = !envEnabled;
    Form1->ButtonService1->Enabled = envEnabled;
    Form1->ButtonService2->Enabled = envEnabled;
    Form1->ButtonService3->Enabled = envEnabled;
}

static void _HexStrToRawData(AnsiString hexStr, uint8_t *resArr, uint16_t len)
{
    uint16_t ind = 0;
    char *strBuf = hexStr.c_str();

    while(ind < len && strlen(strBuf) > 2)
    {
        AnsiString tempStr = "0x" + String(strBuf[0]) + String(strBuf[1]);
        resArr[ind++] = (uint8_t)StrToInt(tempStr);

        while(strlen(strBuf) > 2 &&
              !(strBuf[0] == ' '  && strBuf[1] != ' ' && strBuf[1] != '\r' ||
                strBuf[0] == '\n' && strBuf[1] != ' ' && strBuf[1] != '\r' ))
        {
            strBuf++;
        }
        strBuf++;
    }
}

static void _LbaQtyUpdate()
{
    string driveModel = Form1->ComboBoxDrive1->Text.c_str();
    uint64_t lbaQty = hDriveList->GetLbaQty(driveModel);
    Form1->EditDrive1->Text = lbaQty;
    Form1->EditAccess3->Text = lbaQty;
    Form1->EditService2->Text = lbaQty;
}

static inline void FillDriveComboBox()
{
    TComboBox *ComboBox = Form1->ComboBoxDrive1;

    if (hDriveList != NULL) 
    {
        delete hDriveList;
        hDriveList = NULL;
    }

    hDriveList = new DriveList();
    list<DriveList::Properties_s> propertiesList = hDriveList->GetPropertiesList();

    ComboBox->Items->Clear();

    list<DriveList::Properties_s>::iterator propertiesIt = propertiesList.begin();

    if (propertiesList.size() == 0) {
        ComboBox->Items->Text = "Check drive_ignore_list.txt";
    }

    while (propertiesIt != propertiesList.end())
    {
        ComboBox->Items->Add(propertiesIt->model.c_str());
        ComboBox->ItemIndex++;
        propertiesIt++;
    }
}

static inline void FillUartComboBox()
{
    TComboBox *ComboBox = Form1->ComboBoxUart1;
    ComboBox->Text = "";
	ComboBox->Items->Clear();

	for(uint16_t i = 1; i < 255; i++)
	{
        AnsiString comPortName = "COM" + String(i);
        Uart *curUart = new Uart(comPortName.c_str(), 9600, 0, 0);

		if (curUart->IsOpen())
		{
            ComboBox->Items->Add(comPortName);
            ComboBox->ItemIndex++;
		}

        delete curUart;
	}
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
    int ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int ScreenHeight = GetSystemMetrics(SM_CYSCREEN);
	this->Top = (ScreenHeight/2) - (Height/2);
	this->Left = (ScreenWidth/2) - (Width/2);
    FillUartComboBox();
    FillDriveComboBox();

    _LbaQtyUpdate();
    _ChartClear();
}

//---------------------------------------------------------------------------

void __fastcall TForm1::FormResize(TObject *Sender)
{
    Chart1->Width   = 0;
    Chart1->Left    = _graphFullScrEnable ? 32 : 336;

    Chart1->Height      = Form1->Height - (54 + Chart1->Top);
    Chart1->Width       = Form1->Width  - (49 + Chart1->Left);
    Memo1->Height       = Form1->Height - (54 + Memo1->Top);
    ScrollBar1->Height  = Form1->Height - (55 + ScrollBar1->Top);
    ScrollBar1->Left    = Chart1->Left  - 16;
    ScrollBar2->Height  = Form1->Height - (55 + ScrollBar1->Top);
    ScrollBar2->Left    = Form1->Width  - 54;
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
    if(Button1->Caption == "START")
    {
        AnsiString driveModel = ComboBoxDrive1->Text.c_str();
        uint8_t driveInd = hDriveList->GetIndex((string)driveModel.c_str());

        if(driveInd != DriveList::kInvalidIndex)
        {
            //TODO: "new" without "delete"
            hDrive = new Drive(driveInd);
        }

        if(hDrive != NULL && hDrive->IsOpened())
        {
            hDrive->SetDebugFunc(_PrintMemo);
            Drive::AccessParam_s accessParam;
            accessParam.startLba    = StrToInt64(EditAccess2->Text);
            accessParam.lbaRange    = StrToInt64(EditAccess3->Text);
            accessParam.lbaQty      = StrToInt64(EditAccess3->Text);
            accessParam.itrQty      = StrToInt(EditAccess4->Text);
            accessParam.reqBufSize  = StrToInt(EditAccess1->Text) * 1024;
            accessParam.reqPattern  = StrToInt(EditStat4->Text);
            accessParam.randEn      = RadioButtonAccess2->Checked;
            accessParam.dirWrite    = RadioButtonAccess4->Checked;
            accessParam.markRawLba  = CheckBoxStat1->Checked;
            accessParam.markStrLba  = CheckBoxStat2->Checked,
            accessParam.markTime    = CheckBoxStat3->Checked,
            accessParam.checkRawLba = CheckBoxStat4->Checked;

            if(CheckBoxFind1->Checked)
            {
                uint8_t rawDataBuf[DRIVE_SECT_SIZE];
                _HexStrToRawData(MemoFind1->Text, rawDataBuf, DRIVE_SECT_SIZE);
                hDrive->SetTargetData(rawDataBuf);
            }

            int msgDlgResp = mrYes;

            if(accessParam.dirWrite)
            {
                AnsiString  message = "";
                message += "You are realy want to write \"" + driveModel + "\"?\n";
                message += "\tLBA range: " + String(accessParam.startLba);
                message += " - " + String(accessParam.startLba + accessParam.lbaRange);
                msgDlgResp = MessageDlg(message, mtWarning, TMsgDlgButtons() << mbYes << mbCancel, 0);
            }

            if(msgDlgResp == mrYes)
            {
                _ChartClear();
                hDrive->SetAccessParam(accessParam);
                hDrive->StartTest();
                Form1->Timer1->Enabled = true;
                Button1->Caption = "STOP";
                _TestEnvEnabled(false);
            }
        }
        else
        {
            MessageDlg("Open \"" + driveModel + "\" fail!", mtError, TMsgDlgButtons() << mbOK, 0);
        }
    }
    else
    {
        hDrive->StopTest();
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
    if(Button2->Caption == "Pause")
    {
        hDrive->SuspendTest();
        Button2->Caption = "Resume";
    }
    else
    {
        hDrive->ResumeTest();
        Button2->Caption = "Pause";
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonService1Click(TObject *Sender)
{
    AnsiString driveModel = ComboBoxDrive1->Text;
    uint8_t driveInd = hDriveList->GetIndex((string)driveModel.c_str());
    Drive hDriveLocal(driveInd);

    if(hDriveLocal.IsOpened())
    {
        _PrintMemo("Get SMART from \"%s\"\n", driveModel.c_str());

        smartAttribute_s smartAttributes[SMART_ATTR_MAX_ID];
        hDriveLocal.GetSmart(smartAttributes);

        for(uint8_t attrInd = 0; attrInd < SMART_ATTR_MAX_ID; attrInd++)
        {
            smartAttribute_s *attr = &smartAttributes[attrInd];

            if(attr->Id != SMART_ATTR_INVALID_ID)
            {
                uint64_t rawVal = 0;

                for(uint32_t i = 0; i < 6; i++)
                {
                    rawVal += ((uint64_t)(attr->RawValue[i])) << (i * 8);
                }
                _PrintMemo("ID %3u flags %02X val %3u raw %llu\n", attr->Id, attr->Flags, attr->Value, rawVal);
            }
            else
            {
                break;
            }
        }
    }
    else
    {
        MessageDlg("Open \"" + driveModel + "\" fail!", mtError, TMsgDlgButtons() << mbOK, 0);
    }

}

//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonService2Click(TObject *Sender)
{
    AnsiString driveModel = ComboBoxDrive1->Text.c_str();
    uint8_t driveInd = hDriveList->GetIndex((string)driveModel.c_str());
    Drive hDriveLocal(driveInd);

    if(hDriveLocal.IsOpened())
    {
        uint64_t startLba   = StrToInt64(EditService1->Text);
        uint64_t lbaRange   = StrToInt64(EditService2->Text);

        AnsiString  message = "";
        message += "You are realy want to TRIM \"" + driveModel + "\"?\n";
        message += "\tLBA range: " + String(startLba) + " - " + String(startLba + lbaRange);
        int msgDlgResp = MessageDlg(message, mtWarning, TMsgDlgButtons() << mbYes << mbCancel, 0);

        if(msgDlgResp == mrYes)
        {
            uint64_t startClk = clock();
            hDriveLocal.SetDebugFunc(_PrintMemo);
            hDriveLocal.Trim(startLba, lbaRange);
            _PrintMemo("TRIM done, time left %u ms\n", clock() - startClk);
        }
    }
    else
    {
        MessageDlg("Open \"" + driveModel + "\" fail!", mtError, TMsgDlgButtons() << mbOK, 0);
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonService3Click(TObject *Sender)
{
    AnsiString driveModel = ComboBoxDrive1->Text.c_str();
    uint8_t driveInd = hDriveList->GetIndex((string)driveModel.c_str());
    Drive hDriveLocal(driveInd);

    if(hDriveLocal.IsOpened())
    {
        AnsiString  message = "You are realy want to clean \"" + driveModel + "\"?\n";
        int msgDlgResp = MessageDlg(message, mtWarning, TMsgDlgButtons() << mbYes << mbCancel, 0);

        if(msgDlgResp == mrYes)
        {
            hDriveLocal.SetDebugFunc(_PrintMemo);
            hDriveLocal.Clean();
        }
    }
    else
    {
        MessageDlg("Open \"" + driveModel + "\" fail!", mtError, TMsgDlgButtons() << mbOK, 0);
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonService4Click(TObject *Sender)
{
    AnsiString driveModel = ComboBoxDrive1->Text.c_str();
    uint8_t driveInd = hDriveList->GetIndex((string)driveModel.c_str());
    Drive hDriveLocal(driveInd);

    if(hDriveLocal.IsOpened())
    {
        AnsiString  message = "You are realy want to sanitize block erase \"" + driveModel + "\"?\n";
        int msgDlgResp = MessageDlg(message, mtWarning, TMsgDlgButtons() << mbYes << mbCancel, 0);

        if(msgDlgResp == mrYes)
        {
            hDriveLocal.SetDebugFunc(_PrintMemo);
            hDriveLocal.SendScsiCommand(0xB4, 0x12, 0x426B4572);
        }
    }
    else
    {
        MessageDlg("Open \"" + driveModel + "\" fail!", mtError, TMsgDlgButtons() << mbOK, 0);
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonService5Click(TObject *Sender)
{
    AnsiString driveModel = ComboBoxDrive1->Text.c_str();
    uint8_t driveInd = hDriveList->GetIndex((string)driveModel.c_str());
    Drive hDriveLocal(driveInd);

    if(hDriveLocal.IsOpened())
    {
        AnsiString  message = "You are realy want to sanitize crypto scramble \"" + driveModel + "\"?\n";
        int msgDlgResp = MessageDlg(message, mtWarning, TMsgDlgButtons() << mbYes << mbCancel, 0);

        if(msgDlgResp == mrYes)
        {
            hDriveLocal.SetDebugFunc(_PrintMemo);
            hDriveLocal.SendScsiCommand(0xB4, 0x11, 0x43727970);
        }
    }
    else
    {
        MessageDlg("Open \"" + driveModel + "\" fail!", mtError, TMsgDlgButtons() << mbOK, 0);
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonService6Click(TObject *Sender)
{
    AnsiString driveModel = ComboBoxDrive1->Text.c_str();
    uint8_t driveInd = hDriveList->GetIndex((string)driveModel.c_str());
    Drive hDriveLocal(driveInd);

    if(hDriveLocal.IsOpened())
    {
        hDriveLocal.SetDebugFunc(_PrintMemo);
        hDriveLocal.SendScsiCommand(0xB4, 0x00, 0x00);
    }
    else
    {
        MessageDlg("Open \"" + driveModel + "\" fail!", mtError, TMsgDlgButtons() << mbOK, 0);
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonService7Click(TObject *Sender)
{
    AnsiString driveModel = ComboBoxDrive1->Text.c_str();
    uint8_t driveInd = hDriveList->GetIndex((string)driveModel.c_str());
    Drive hDriveLocal(driveInd);

    if(hDriveLocal.IsOpened())
    {
        IDENTIFY_DEVICE_DATA identify;
        hDriveLocal.SetDebugFunc(_PrintMemo);
        hDriveLocal.GetIdentify(&identify);
    }
    else
    {
        MessageDlg("Open \"" + driveModel + "\" fail!", mtError, TMsgDlgButtons() << mbOK, 0);
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
    FreeConsole();
    if (AllocConsole())
    {
        int hCrt = _open_osfhandle((long) GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
        FILE *file = _fdopen(hCrt, "w");
        if(setvbuf(file, NULL, _IONBF, 0) == 0)
        {
            *stdout = *file;
            *stderr = *file;
        }
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ButtonUart1Click(TObject *Sender)
{
    bool envEnabled;

    if(ButtonUart1->Caption == "Open")
    {
        AnsiString comPortName  = ComboBoxUart1->Text;
        uint32_t baudrate       = StrToInt(ComboBoxUart2->Text);
        uint8_t parity          = (uint8_t)ComboBoxUart3->ItemIndex;
        uint8_t stopBits        = (uint8_t)ComboBoxUart4->ItemIndex;

        hUart = new Uart(comPortName.c_str(), baudrate, parity, stopBits);

        ButtonUart1->Caption = "Close";
        envEnabled = false;
    }
    else
    {
        delete hUart;
        hUart = NULL;

        ButtonUart1->Caption = "Open";
        envEnabled = true;
    }

    ComboBoxUart1->Enabled = envEnabled;
    ComboBoxUart2->Enabled = envEnabled;
    ComboBoxUart3->Enabled = envEnabled;
    ComboBoxUart4->Enabled = envEnabled;
    EditUart1->Enabled = envEnabled;
}

//---------------------------------------------------------------------------

 void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
    uint64_t curClock = hDrive->GetTimeLeftMs();
    double curTime_sec = (double)(curClock + 1) / CLOCKS_PER_SEC;

#if (CLOCKS_PER_SEC != 1000)
#error
#endif

    double curProcessedMb = (double)hDrive->GetProcessedSize() / (double)(1024 * 1024);
    double curCheckFailMb = (double)hDrive->GetCheckFailSize() / (double)(1024 * 1024);
    double speedSlice  = (double)hDrive->GetSpeedSlice() / (double)(1024 * 1024);

    double xVal = 0;

    if(RadioButtonGraph1->Checked)
    {
        xVal = curTime_sec;
    }
    else if(RadioButtonGraph2->Checked)
    {
        xVal = curProcessedMb;
    }
    else if(RadioButtonGraph3->Checked)
    {
        xVal = (curProcessedMb * 2 * 1024.0) / 1000.0;
    }

    if(RadioButtonAccess4->Checked)
    {
        _AddSeriesXY(SERIES_TYPE_WRITE, xVal, speedSlice);
    }
    else
    {
        _AddSeriesXY(SERIES_TYPE_READ, xVal, speedSlice);
    }

    EditStat1->Text = _SecToTime(curTime_sec);
    EditStat2->Text = FloatToStrF(curProcessedMb, ffNumber, 10, 3) + " MB";
    EditStat3->Text = FloatToStrF(curProcessedMb / curTime_sec, ffNumber, 10, 3) + " MB/s";
    EditStat5->Text = "0x" + IntToHex((__int64)hDrive->GetLastData(), 8);
    EditStat6->Text = FloatToStrF(curCheckFailMb, ffNumber, 10, 3) + " MB";

    if(CheckBoxGraph1->Checked)
    {
        static uint64_t temperatureUpdateClock = 0;
        if(temperatureUpdateClock + 1000 < clock())
        {
            _AddSeriesXY(SERIES_TYPE_TEMPERATURE, xVal, hDrive->GetTemperature());
            temperatureUpdateClock = clock();
        }
    }

    if(CheckBoxGraph2->Checked)
    {
        uint8_t uartValIndex = (uint8_t)StrToInt(EditUart1->Text);

        if (hUart != NULL && hUart->IsOpen())
        {
            int32_t uartVal = hUart->GetVal(uartValIndex);
            Series7->AddXY(xVal, uartVal);
        }
    }

    if(hDrive->IsTestDone())
    {
        Timer1->Enabled = false;
        Button1->Caption = "START";
        Button2->Caption = "Pause";
        _TestEnvEnabled(true);
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ComboBoxDrive1DropDown(TObject *Sender)
{
    FillDriveComboBox();
    _LbaQtyUpdate();
}

 void __fastcall TForm1::ComboBoxDrive1Change(TObject *Sender)
{
    _LbaQtyUpdate();
}

void __fastcall TForm1::ComboBoxDrive1DblClick(TObject *Sender)
{
    TForm1::ComboBoxDrive1DropDown(NULL);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButtonGraph1Click(TObject *Sender)
{
    Chart1->BottomAxis->Title->Caption = "Time [s]";
    EditGraph2->Text = 20;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButtonGraph2Click(TObject *Sender)
{
    Chart1->BottomAxis->Title->Caption = "Read / Written [MB]";
    EditGraph2->Text = 1024;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RadioButtonGraph3Click(TObject *Sender)
{
    Chart1->BottomAxis->Title->Caption = "Read / Written [LBAs x1000]";
    EditGraph2->Text = 2048;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::EditAccess3DblClick(TObject *Sender)
{
    EditAccess3->Text = EditDrive1->Text;
}

//---------------------------------------------------------------------------

void __fastcall TForm1::EditService2DblClick(TObject *Sender)
{
    EditService2->Text = (uint64_t)(StrToInt64(EditDrive1->Text));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ComboBoxUart1DropDown(TObject *Sender)
{
    FillUartComboBox();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ScrollBar1Change(TObject *Sender)
{
    Chart1->LeftAxis->Maximum = ScrollBar1->Position;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ScrollBar2Change(TObject *Sender)
{
    Chart1->RightAxis->Maximum = ScrollBar2->Position;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit4DblClick(TObject *Sender)
{
    EditStat4->Text = "0x1A2B3C4D";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBoxGraph2Click(TObject *Sender)
{
    Chart1->RightAxis->Visible = CheckBoxGraph2->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Chart1DblClick(TObject *Sender)
{
    _graphFullScrEnable ^= true;
    GroupBox1->Visible ^= true;
    TForm1::FormResize(NULL);   
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if (GetAsyncKeyState(VK_MENU) < 0)
    {
        int ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
        int ScreenHeight = GetSystemMetrics(SM_CYSCREEN);

        switch (Key)
        {
            case VK_RETURN:
                this->BorderStyle   = bsSizeable;
                this->Width         = this->Constraints->MinWidth;
                this->Height        = this->Constraints->MinHeight;
	            this->Top           = (ScreenHeight / 2) - (this->Height / 2);
	            this->Left          = (ScreenWidth / 2) - (this->Width / 2);
                break;

            case VK_UP:
                this->BorderStyle   = bsNone;
                this->Width         = ScreenWidth;
                this->Height        = 520;
	            this->Top           = 0;
	            this->Left          = 0;
                Chart1->Left        = _graphFullScrEnable ? 20 : 320;
                Chart1->Height      = Form1->Height - (20 + Chart1->Top);
                Chart1->Width       = Form1->Width  - (30 + Chart1->Left);
                Memo1->Height       = Form1->Height - (20 + Memo1->Top);
                ScrollBar1->Height  = Form1->Height - (20 + ScrollBar1->Top);
                ScrollBar1->Left    = Chart1->Left;
                ScrollBar2->Height  = Form1->Height - (20 + ScrollBar1->Top);
                ScrollBar2->Left    = Form1->Width  - 40;
                break;

            case VK_DOWN:
                this->BorderStyle   = bsNone;
                this->Width         = ScreenWidth;
                this->Height        = 520;
	            this->Top           = 520;
	            this->Left          = 0;
                Chart1->Left        = _graphFullScrEnable ? 20 : 320;
                Chart1->Height      = Form1->Height - (20 + Chart1->Top);
                Chart1->Width       = Form1->Width  - (30 + Chart1->Left);
                Memo1->Height       = Form1->Height - (20 + Memo1->Top);
                ScrollBar1->Height  = Form1->Height - (20 + ScrollBar1->Top);
                ScrollBar1->Left    = Chart1->Left;
                ScrollBar2->Height  = Form1->Height - (20 + ScrollBar1->Top);
                ScrollBar2->Left    = Form1->Width  - 40;
                break;
        }
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormMouseDown(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
    long SC_DRAGMOVE = 0xF012;
    if(Button == mbLeft)
    {
        ReleaseCapture();
        SendMessage(Handle, WM_SYSCOMMAND, SC_DRAGMOVE, 0);
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::EditGraph3Change(TObject *Sender)
{
    int timerInterval = 64;
    TryStrToInt(Form1->EditGraph3->Text, timerInterval);
    Form1->Timer1->Interval = (timerInterval == 0) ? 1 : timerInterval;
}
//---------------------------------------------------------------------------

